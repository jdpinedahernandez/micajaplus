package com.servipunto.micajaplus.json;

import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.ProductDTO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan on 15/05/2018.
 */

public class Product {
    public static List<DTO> getProductData(String respondsFeed) {

        List<DTO> productList = new ArrayList<DTO>();
        try {
            JSONObject rootJSONObj = new JSONObject(respondsFeed);
            JSONArray dataJSONARRAy = rootJSONObj.getJSONArray("data");
            for (int i = 0; i < dataJSONARRAy.length(); i++) {
                JSONObject productINFO = dataJSONARRAy.getJSONObject(i);
                ProductDTO productDTO = new ProductDTO();
                productDTO.setModifiedDate(productINFO.getString("modified_date"));
                productDTO.setCreateDate(productINFO.getString("create_date"));
                productDTO.setName(productINFO.getString("name"));
                productDTO.setUom(productINFO.getString("uom"));
                productDTO.setSupplierId(productINFO.getString("supplier_code"));
                productDTO.setBarcode(productINFO.getString("barcode"));
                productDTO.setLineId("" + productINFO.getString("line_id"));
                productDTO.setActiveStatus(Integer.parseInt(productINFO.getString("active_status")));
                productDTO.setSellingPrice("" + productINFO.getDouble("selling_price"));
                try {
                    productDTO.setQuantity("" + productINFO.getLong("quantity"));
                } catch (Exception e) {
                    productDTO.setQuantity("0");
                }
                productDTO.setGroupId("" + productINFO.getString("group_id"));
                productDTO.setPurchasePrice("" + productINFO.getDouble("purchase_price"));
                productDTO.setVat(productINFO.getString("vat"));
                productDTO.setProductId(productINFO.getString("barcode"));
                productDTO.setProductFlag(productINFO.getString("product_source"));
                productDTO.setExpiry_date(productINFO.getString("expiry_date"));
                productDTO.setDiscount(productINFO.getString("discount"));
                // try {
                // productDTO.setMin_count_inventory(productINFO
                // .getString("min_count_inventory"));
                // } catch (Exception e) {
                // productDTO.setMin_count_inventory("0");
                // }
                if (productINFO.get("min_count_inventory").equals("null")) {
                    productDTO.setMin_count_inventory("0");

                } else {
                    productDTO.setMin_count_inventory(productINFO.getString("min_count_inventory"));
                }
                productDTO.setSyncStatus(1);
                productDTO.setFecha_inicial(productINFO.getString("fecha_inicial"));
                productDTO.setFecha_final(productINFO.getString("fecha_final"));
                try {
                    if ("" != productINFO.getString("barcode") && "" != productINFO.getString("supplier_code")) {
                        productList.add(productDTO);
                    }
                } catch (Exception e) {

                }
                if (productINFO.get("discount").equals("null")) {
                    productDTO.setDiscount("0");

                } else {
                    productDTO.setDiscount(productINFO.getString("discount"));
                }
                if (productINFO.get("favorite").equals("null")) {
                    productDTO.setFavorite("0");

                } else {
                    productDTO.setFavorite(productINFO.getString("favorite"));
                }

            }

        } catch (Exception e) {
//            ServiApplication.connectionTimeOutState = false;
        }
        return productList;
    }
}

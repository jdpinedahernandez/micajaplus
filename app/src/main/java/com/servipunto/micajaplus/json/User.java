package com.servipunto.micajaplus.json;

import android.content.Context;
import android.content.SharedPreferences;

import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.UserDetailsDTO;
import com.servipunto.micajaplus.database.dto.UserModuleIdDTO;
import com.servipunto.micajaplus.utils.Constants;
import com.servipunto.micajaplus.utils.Dates;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan on 14/05/2018.
 */

public class User {

    public static List<DTO> setDataUserInfo(String responds_feed,SharedPreferences.Editor editor) {


        List<DTO> userInfo = new ArrayList<DTO>();
        try {

            JSONObject jsonObject = new JSONObject(responds_feed);
            JSONObject jsonuserJsonObject = jsonObject.getJSONObject("user");
            UserDetailsDTO userDetails = new UserDetailsDTO();

                userDetails.setCedulaDocument(jsonuserJsonObject.getString("cedula_document"));

                userDetails.setCloseDateTime(jsonuserJsonObject.getString("closing_time"));
                userDetails.setCompanyName(jsonuserJsonObject.getString("company_name"));
                userDetails.setImei(jsonuserJsonObject.getString("imei"));
                userDetails.setIsClosed(Constants.TRUE);
                userDetails.setLastLogin(Dates.currentdate());
                userDetails.setName(jsonuserJsonObject.getString("name"));
                userDetails.setNitShopId(jsonuserJsonObject.getString("store_code"));
                userDetails.setOpeningBalance(Dates.currentdate());
                userDetails.setOpeningDateTime(jsonuserJsonObject.getString("opening_date_time"));
                //userDetails.setPassword(jsonuserJsonObject.getString("password"));
                userDetails.setRegistrationDate(jsonuserJsonObject.getString("registration_date"));
                userDetails.setIs_admin(jsonuserJsonObject.getString("is_admin"));
                userDetails.setIs_authorized(jsonuserJsonObject.getString("is_authorized"));
                userDetails.setAuthtoken(jsonuserJsonObject.getString("authtoken"));

                if (jsonuserJsonObject.getString("is_admin").equals("null")) {
                    if (jsonuserJsonObject.getString("is_authorized").equals("null")) {
                        userDetails.setIs_admin("Y");
                    }
                }

                userDetails.setEmail(jsonuserJsonObject.getString("email"));
                try {
                    userDetails.setTerminalId(jsonuserJsonObject.getString("terminalId"));
                    userDetails.setPuntoredId(jsonuserJsonObject.getString("puntoDeVentaId"));
                    userDetails.setComercioId(jsonuserJsonObject.getString("comercioId"));
                    userDetails.setSystemId("1");
                } catch (Exception e) {
                    userDetails.setTerminalId("");
                    userDetails.setPuntoredId("");
                    userDetails.setComercioId("");
                    userDetails.setSystemId("");
                }
                try {
                    userDetails.setActualBalance(""
                            + jsonuserJsonObject.getDouble("actual_balance"));
                } catch (Exception e) {
                    userDetails.setActualBalance("" + 00.0);

                }
                userDetails.setSyncStatus(Constants.FALSE);
                userDetails.setUserId(jsonuserJsonObject.getString("user_id"));
                userDetails.setUserName(jsonuserJsonObject.getString("username"));
                userDetails.setPassword(jsonuserJsonObject.getString("password"));
                userInfo.add(userDetails);
                editor.putString("username", userDetails.getUserName());
                editor.putString("store_code", userDetails.getNitShopId());
                editor.commit();
                System.out.println("into if user info");
                return userInfo;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public static List<DTO> getModulesData(String respondsFeed) {

        List<DTO> productList = new ArrayList<DTO>();
        try {
            JSONObject rootJSONObj = new JSONObject(respondsFeed);
            JSONArray dataJSONARRAy = rootJSONObj.getJSONArray("modules");
            for (int i = 0; i < dataJSONARRAy.length(); i++) {
                JSONObject productINFO = dataJSONARRAy.getJSONObject(i);
                UserModuleIdDTO userModuleIdDTO = new UserModuleIdDTO();
                userModuleIdDTO.setModuleCode(productINFO.getString("module_code"));
                userModuleIdDTO.setModuleName(productINFO.getString("name"));
                productList.add(userModuleIdDTO);
                System.out.println("Module Code " + userModuleIdDTO.getModuleCode());
                System.out.println("Module Name " + userModuleIdDTO.getModuleName());
                System.out.println("Module id " + userModuleIdDTO.getUserModuleId());
            }
            return productList;

        } catch (Exception e) {
            return productList;
        }
    }

}

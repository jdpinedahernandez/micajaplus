package com.servipunto.micajaplus.json;

import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.InventoryDTO;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Juan on 15/05/2018.
 */

public class Inventory {
    public static List<DTO> getinventory(String respondsFeed) {

        List<DTO> inventorylist = new ArrayList<DTO>();
        try {
            JSONObject rootJSONObj = new JSONObject(respondsFeed);
            JSONArray dataJSONARRAy = rootJSONObj.getJSONArray("data");
            for (int i = 0; i < dataJSONARRAy.length(); i++) {
                JSONObject productINFO = dataJSONARRAy.getJSONObject(i);
                InventoryDTO productDTO = new InventoryDTO();
                if (productINFO.getLong("inventory_id")==0){
                    Random rn = new Random();
                    productDTO.setInventoryId(String.valueOf(rn.nextInt(100))+String.valueOf(rn.nextInt(10000))
                            +String.valueOf(rn.nextInt(10000))+String.valueOf(rn.nextInt(10000)));
                    productDTO.setSyncStatus(0);
                }
                else {
                    productDTO.setInventoryId("" + productINFO.getLong("inventory_id"));
                    productDTO.setSyncStatus(1);
                }
                productDTO.setProductId(productINFO.getString("barcode"));
                productDTO.setUom(productINFO.getString("uom"));
                productDTO.setQuantity(productINFO.getString("quantity"));
//				productDTO.setSyncStatus(1);
                if (null != productINFO.getString("barcode")) {
                    inventorylist.add(productDTO);
                }
            }

        } catch (Exception e) {
        }
        return inventorylist;
    }
}

package com.servipunto.micajaplus;

/**
 * Created by Juan on 19/05/2018.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.servipunto.micajaplus.adapters.ProductsPagerAdapter;
import com.servipunto.micajaplus.adapters.RecycleAdapter_AddProduct;
import com.servipunto.micajaplus.database.DBHandler;
import com.servipunto.micajaplus.database.dao.InventoryDAO;
import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.SelectedProddutsDTO;
import com.servipunto.micajaplus.interfaces.CustomItemClickListener;
import com.servipunto.micajaplus.service.SQLiteSale;
import com.servipunto.micajaplus.utils.Constants;
import com.servipunto.micajaplus.utils.ConstantsEnum;

import java.util.ArrayList;
import java.util.List;


public class ContentActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    RecycleAdapter_AddProduct mAdapter;
    private ProductsPagerAdapter productsPagerAdapter;
    private TabLayout customTab;
    private ViewPager pager;
    Typeface mTypeface;
    private Dialog bottomDialog;
    private LinearLayout linearDialog;
    private EditText edt_productos;
    private TextView total_txt;
    private FrameLayout frameOk;
    private ArrayList<String> stringArrayList = new ArrayList<>();
    private ImageView btn;
    Animation slideUpAnimation;
    List<DTO> selectedproductdtolist = new ArrayList<>();
    public static ServiApplication appContext;
    double total=0;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        appContext = (ServiApplication) getApplicationContext();
        slideUpAnimation= AnimationUtils.loadAnimation(ContentActivity.this, R.anim.slide_up_linear);
        sharedpreferences = getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);

        bottomDialog  = new Dialog(ContentActivity.this,R.style.BottomDialog);
        bottomDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        bottomDialog.getWindow().getAttributes().windowAnimations = R.style.CustomDialogAnimation;
        layoutParams.copyFrom(bottomDialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.gravity = Gravity.BOTTOM;
        bottomDialog.getWindow().setAttributes(layoutParams);
        bottomDialog.setCancelable(true);
        bottomDialog.setContentView(R.layout.dilaog_bottom);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        edt_productos = (EditText) findViewById(R.id.edt_productos);
        total_txt = (TextView) bottomDialog.findViewById(R.id.total);
        frameOk= (FrameLayout) bottomDialog.findViewById(R.id.frameOk);
        linearDialog= (LinearLayout) bottomDialog.findViewById(R.id.linearDialog);
        RecyclerView.LayoutManager mLayoutManagerHappyHours = new LinearLayoutManager(ContentActivity.this,
                LinearLayoutManager.HORIZONTAL, false);

        btn = (ImageView)findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double total_cantidades=0;
                double total_precio=0;
                for (int j=0;j<appContext.getSelectedproductdtolist().size();j++){
                    SelectedProddutsDTO selectedProddutsDTOl=(SelectedProddutsDTO)appContext.getSelectedproductdtolist().get(j);
                    total_cantidades=Double.parseDouble(selectedProddutsDTOl.getActualQty());
                    total+=Double.parseDouble(selectedProddutsDTOl.getSellPrice())*total_cantidades;
                }
                total_txt.setText("Total: "+String.valueOf(total));
                bottomDialog.show();
                linearDialog.startAnimation(slideUpAnimation);
            }
        });

        frameOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String payments=total+":0:false:0:"+total+":"+ ConstantsEnum.PAYMENT_TYPE_CASH.code();
                new SQLiteSale(appContext,sharedpreferences,payments,appContext.getSelectedproductdtolist());
                appContext.setSelectedproductdtolist(new ArrayList<DTO>());
                appContext.getmAdapter().setList(appContext.getSelectedproductdtolist());
                appContext.getmAdapter().notifyDataSetChanged();
                bottomDialog.dismiss();
            }
        });
        pager = (ViewPager)findViewById(R.id.pager);
        customTab= (TabLayout) findViewById(R.id.customTab);
        customTab.addTab(customTab.newTab().setText("Venta: "+total));
/*            customTab.addTab(customTab.newTab().setText("Women's Wears"));
            customTab.addTab(customTab.newTab().setText("HouseHolds"));*/
        mTypeface = Typeface.createFromAsset(this.getAssets(), "myfonts/Roboto-Medium.ttf");
        ViewGroup vg = (ViewGroup) customTab.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(mTypeface, Typeface.NORMAL);
                }
            }
        }


        productsPagerAdapter = new ProductsPagerAdapter
                (getSupportFragmentManager(), customTab.getTabCount());
        pager.setAdapter(productsPagerAdapter);
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(customTab));
        pager.setOffscreenPageLimit(customTab.getTabCount());

        edt_productos.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (edt_productos.getText().toString().trim().length() > 2 &&
                        (edt_productos.getText().toString().matches("[a-zA-Z ]+"))) {
                    CargarProductoVistaXNombre();
                }
                else if (edt_productos.getText().toString().trim().length() > 2 &&
                        edt_productos.getText().toString().matches("[0-9]+")) {
                    CargarProductoVistaXCodBarras();
                }
                else{
                    mAdapter.setList(new ArrayList<DTO>());
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mAdapter = new RecycleAdapter_AddProduct(ContentActivity.this,selectedproductdtolist,AnimationUtils.loadAnimation(ContentActivity.this, R.anim.bounce), new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, SelectedProddutsDTO selectedProddutsDTO) {
                edt_productos.setText("");
                mAdapter.setList(new ArrayList<DTO>());
                mAdapter.notifyDataSetChanged();
                double cantidad;
                boolean existe = false;
                cantidad = Double.parseDouble(selectedProddutsDTO.getActualQty())+1;
                double qty = 0;
                for (int j=0;j<appContext.getSelectedproductdtolist().size();j++){
                    SelectedProddutsDTO selectedProddutsDTOl=(SelectedProddutsDTO)appContext.getSelectedproductdtolist().get(j);
                    if (selectedProddutsDTO.getIdProduct().equalsIgnoreCase(selectedProddutsDTOl.getIdProduct())){
                        qty = Double.parseDouble(selectedProddutsDTOl.getActualQty())+1;
                        selectedProddutsDTO.setActualQty(String.valueOf(qty));
                        appContext.getSelectedproductdtolist().remove(j);
                        appContext.getSelectedproductdtolist().add(0,selectedProddutsDTO);
                        existe=true;
                    }
                }
                if (!existe){
                    selectedProddutsDTO.setActualQty("1");
                    appContext.getSelectedproductdtolist().add(0,selectedProddutsDTO);
                }
                appContext.getmAdapter().setList(appContext.getSelectedproductdtolist());
                appContext.getmAdapter().notifyDataSetChanged();
            }
        });
    }

    private void CargarProductoVistaXNombre(){
        selectedproductdtolist = InventoryDAO.getInstance().getProductDetailsWithBarcode3(
                DBHandler.getDBObj(Constants.READABLE),edt_productos.getText().toString().trim(),10);
        mAdapter.setList(selectedproductdtolist);
        mAdapter.notifyDataSetChanged();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ContentActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }
    private void CargarProductoVistaXCodBarras(){
        selectedproductdtolist = InventoryDAO.getInstance().getProductDetailsWithBarcode2(
                DBHandler.getDBObj(Constants.READABLE),edt_productos.getText().toString().trim(),10);
        mAdapter.setList(selectedproductdtolist);
        mAdapter.notifyDataSetChanged();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ContentActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }
}

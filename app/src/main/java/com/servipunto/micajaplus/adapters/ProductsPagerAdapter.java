package com.servipunto.micajaplus.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.servipunto.micajaplus.SalesActivity;

/**
 * Created by wolfsoft on 10/11/2015.
 */
public class ProductsPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;


    public ProductsPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }


    @Override
    public Fragment getItem(int position) {

        SalesActivity fragment = new SalesActivity();
        return fragment;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }


}
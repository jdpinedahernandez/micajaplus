package com.servipunto.micajaplus.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.servipunto.micajaplus.R;
import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.SelectedProddutsDTO;
import com.servipunto.micajaplus.interfaces.CustomItemClickListener;

import java.util.List;

/**
 * Created by Juan on 19/05/2018.
 */

public class RecycleAdapter_AddProduct extends RecyclerView.Adapter<RecycleAdapter_AddProduct.MyViewHolder> {

    Context context;
    boolean showingFirst = true;
    private List<DTO> selectedproductdtolist;
    Animation startAnimation;
    int recentPos = -1;
    int row_index = -1;
    CustomItemClickListener listener;
    double total_price = 0.0;

    public void setList(List<DTO> selectedproductdtolist) {
        this.selectedproductdtolist = selectedproductdtolist;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView title;
        TextView price;
        TextView quantityTxt;
        private LinearLayout llMinus,llPlus,contenedor;
        double quantity = 0.0;

        public MyViewHolder(View view) {
            super(view);

            image = (ImageView) view.findViewById(R.id.image);
            title = (TextView) view.findViewById(R.id.title);
            price = (TextView) view.findViewById(R.id.price);
            quantityTxt = (TextView) view.findViewById(R.id.quantityTxt);
            llPlus = (LinearLayout)view.findViewById(R.id.llPlus);
            llMinus = (LinearLayout)view.findViewById(R.id.llMinus);
            contenedor = (LinearLayout)view.findViewById(R.id.contenedor);
        }

    }



    public RecycleAdapter_AddProduct(Context context, List<DTO> selectedproductdtolist,Animation startAnimation, CustomItemClickListener listener) {
        this.selectedproductdtolist = selectedproductdtolist;
        this.context = context;
        this.startAnimation=startAnimation;
        this.listener = listener;
    }

    @Override
    public RecycleAdapter_AddProduct.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_add_product, parent, false);

        return new RecycleAdapter_AddProduct.MyViewHolder(itemView);
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final RecycleAdapter_AddProduct.MyViewHolder holder, final int position) {

        final SelectedProddutsDTO selectedproductdto = (SelectedProddutsDTO) selectedproductdtolist
                .get(position);
        holder.title.setText(selectedproductdto.getName());
        holder.price.setText(selectedproductdto.getSellPrice());
        holder.quantityTxt.setText(selectedproductdto.getActualQty());
        holder.quantity = Double.parseDouble(selectedproductdto.getActualQty());
        total_price+= holder.quantity * Double.parseDouble(selectedproductdto.getSellPrice());


        if (position == recentPos) {
            Log.e("pos", "" + recentPos);
            // start animation
            holder.quantityTxt.startAnimation(startAnimation);

        } else {
            holder.quantityTxt.clearAnimation();

        }


        if (Double.parseDouble(selectedproductdto.getActualQty()) > 0){
            holder.quantityTxt.setVisibility(View.VISIBLE);
            holder.llMinus.setVisibility(View.VISIBLE);
        }else {
            holder.quantityTxt.setVisibility(View.GONE);
            holder.llMinus.setVisibility(View.GONE);
        }

        holder.llPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                recentPos = position;
                holder.quantity = holder.quantity + 1;
                selectedproductdto.setActualQty(String.valueOf(holder.quantity));
                holder.quantityTxt.setText("" + holder.quantity);
                notifyDataSetChanged();

            }
        });


        holder.llMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.quantity > 0 ){
                    recentPos = position;
                    holder.quantity = holder.quantity - 1;
                    selectedproductdto.setActualQty(String.valueOf(holder.quantity));
                    holder.quantityTxt.setText("" + holder.quantity);
                }
                notifyDataSetChanged();

            }
        });

        holder.contenedor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v,selectedproductdto);
            }
        });


    }

    @Override
    public int getItemCount() {
        return selectedproductdtolist.size();
    }

}

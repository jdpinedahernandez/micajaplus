package com.servipunto.micajaplus.utils;

/**
 * Created by Juan on 18/05/2018.
 */

public enum ConstantsEnum {

    ADD_CLIENT("add client"),STORE_CODE("store_code"), EDIT_CLIENT("edit client"), CLIENT_MODE(
            "client mode"), CLIENT_ID("Client id"), CLIENT_NAME("Client name"), CLIENT_TELEPHONE(
            "Client number"), ADD_SUPPLIER("Add Supplier"), EDIT_SUPPLIER(
            "Edit Supplier"), SUPPLIER_MODE("Supplier mode"), SUPPLIER_ID(
            "Supplier id"), INVENTORY("inventory"), CASH_DEPOSIT("Cash Deposit"), CASH_WITHDRAW(
            "Cash Withdraw"), PAYMENT_TYPE_CASH("Cash"), PAYMENT_TYPE_CARD(
            "card"), PAYMENT_TYPE_CREDIT("credit"), INCOME_TYPE_SALE("sale"),INCOME_TYPE_SALES("Venta"), INCOME_TYPE_SALE_RECHARGE("Recarga"), INCOME_TYPE_DEBIT(
            "debit"), CLIENT_PAY("clientpay"), CLIENT_LEND("lendmoney"), SUPPLIER_PAY(
            "suppay"), SUPPLIER_PURCHASE_TYPE("Supplier Debit"), CASH_TYPE_WITHDRAW(
            "cash withdraw"), CASH_TYPE_DEPOSIT("cash Deposit"), CLIENT_CEDULA(
            "cedula"), SUPPLIER_PAYMENT("Supp  Expenses"), CASH_SHOP_OPEN(
            "shop open cash"),ORDERS("Orders"),EDIT_PRODUCT("edit product"),ADD_PRODUCT("add product"),ADD_PRODUCT2("add product2"),PRODUCT_MODE("edit product"),PRODUCT_ID("product id"),PRODUCT_SUPPLIER("product supplier"),
    SALES("Sales"),WEEKLY("Weekly"),OCCASIONALLY("Occasionally"),DAILY("Daily"),DISH("Dish"),
    EDIT_DISH("Edit Dish"),VIEW_MENUS("View Menus"),VIEW_DISH("View Dish"),SELECTION("Selectio"),VIEW("View"),PRODUCT_DISH("product dish"),PRODUCT_NON_DISH("product non dish"),INVOICE("invoice"),
    RECEIVE_INVENTORY("receive_y"),
    PREVIOUS_ACITVITY("previous_activity");

    private String name;

    private ConstantsEnum(String name) {
        this.name = name;
    }

    public String code() {
        return name;
    }
}
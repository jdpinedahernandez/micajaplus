/*******************************************************************************
 *  @author 
 *  Ybrant Digital
 *  Copyright (C) Ybrant Digital
 *  http://www.ybrantdigital.com
 *******************************************************************************/
package com.servipunto.micajaplus.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.servipunto.micajaplus.database.dto.DTO;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class Utils {

	public static Context context;
	public static String saleID="";
	private static List<DTO> invoiceList = new ArrayList<DTO>();

	public static List<DTO> getInvoiceList() {
		return invoiceList;
	}

	public static void setInvoiceList(List<DTO> invoiceList) {
		Utils.invoiceList = invoiceList;
	}

	public static void clearEditText(ViewGroup viewGroup) {
		for (int i = 0, count = viewGroup.getChildCount(); i < count; ++i) {
			View view = viewGroup.getChildAt(i);
			if (view instanceof EditText) {
				((EditText) view).setText("");
			}

			if (view instanceof ViewGroup
					&& (((ViewGroup) view).getChildCount() > 0))
				clearEditText((ViewGroup) view);
		}
	}
}

package com.servipunto.micajaplus.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.servipunto.micajaplus.database.DBHandler;
import com.servipunto.micajaplus.database.dao.InventoryDAO;
import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.InventoryDTO;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * Created by Juan on 18/05/2018.
 */

public class CommonMethods {
    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    public static double getDoubleFormate(String input) {
        NumberFormat format = NumberFormat.getInstance(Locale.ENGLISH);
        Number number = null;
        double d = 0;
        if (!isDouble(input)) {
            try {
                number = format.parse(input);
                d = number.doubleValue();
            } catch (ParseException e) {
                d = 0.0;
            }
        } else
            d = Double.valueOf(input);

        return d;
    }
    private static boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
    public static String getRoundedVal(double value) {

		/*
		 * DecimalFormat format = new DecimalFormat("0.00"); return
		 * format.format(value);
		 */

        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        DecimalFormat format = new DecimalFormat("0.00", dfs);

        Locale L = new Locale("en");
        Locale.setDefault(L);

        return format.format(value);
    }
    public static String UOMConversions(String qtyFromTable, String uomFromTable, String qtyFromList,
                                        String uomFromList, String selDishQty, String type) {
        double finalQty = 0;
        if (uomFromTable.equalsIgnoreCase("kg") || uomFromTable.equalsIgnoreCase("lt")) {
            if (uomFromList.equalsIgnoreCase("kg") || uomFromList.equalsIgnoreCase("lt")) {
                if (type.equals(SalesEditTypes.INVOICE_ADJUSTMENT.code()))
                    finalQty = getDoubleFormate(qtyFromTable)
                            + (getDoubleFormate(qtyFromList) * getDoubleFormate(selDishQty));
                else
                    finalQty = getDoubleFormate(qtyFromTable)
                            - (getDoubleFormate(qtyFromList) * getDoubleFormate(selDishQty));
            } else if (uomFromList.equalsIgnoreCase("gm") || uomFromList.equalsIgnoreCase("ml")) {
                double kgToGm = getDoubleFormate(qtyFromTable) * 1000;
                if (type.equals(SalesEditTypes.INVOICE_ADJUSTMENT.code()))
                    finalQty = kgToGm + (getDoubleFormate(qtyFromList) * (getDoubleFormate(selDishQty)));
                else
                    finalQty = kgToGm - (getDoubleFormate(qtyFromList) * (getDoubleFormate(selDishQty)));
                finalQty = finalQty * 0.001;
            }
        } else if (uomFromTable.equalsIgnoreCase("gm") || uomFromTable.equalsIgnoreCase("ml")) {
            if (uomFromList.equalsIgnoreCase("kg") || uomFromList.equalsIgnoreCase("lt")) {
                double kgToGm = getDoubleFormate(qtyFromList) * 1000;
                if (type.equals(SalesEditTypes.INVOICE_ADJUSTMENT.code()))
                    finalQty = getDoubleFormate(qtyFromTable) + (kgToGm * getDoubleFormate(selDishQty));
                else
                    finalQty = getDoubleFormate(qtyFromTable) - (kgToGm * getDoubleFormate(selDishQty));
            } else if (uomFromList.equalsIgnoreCase("gm") || uomFromList.equalsIgnoreCase("ml")) {
                if (type.equals(SalesEditTypes.INVOICE_ADJUSTMENT.code()))
                    finalQty = getDoubleFormate(qtyFromTable)
                            + (getDoubleFormate(qtyFromList) * getDoubleFormate(selDishQty));
                else
                    finalQty = getDoubleFormate(qtyFromTable)
                            - (getDoubleFormate(qtyFromList) * getDoubleFormate(selDishQty));
            }
        } else
            finalQty = CommonMethods.getDoubleFormate(qtyFromTable) - CommonMethods.getDoubleFormate(qtyFromList);
        return String.valueOf(CommonMethods.getRoundedVal(finalQty));
    }
    public static double getBalanceQuantity(String barcode, String Quantity) {
        try {
            InventoryDTO inventoryDTO = InventoryDAO.getInstance().getInventoryValue(DBHandler.getDBObj(Constants.READABLE), barcode);
            double currentBalanceQuantity = inventoryDTO.getQuantityBalance();
            return currentBalanceQuantity + CommonMethods.getDoubleFormate(Quantity);
        } catch (Exception e) {
            e.printStackTrace();
            return -1.0;
        }
    }
}

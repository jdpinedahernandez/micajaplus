package com.servipunto.micajaplus.utils;

import android.content.ContentValues;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.servipunto.micajaplus.ServiApplication;
import com.servipunto.micajaplus.database.DBHandler;
import com.servipunto.micajaplus.database.dao.SincronizarTransaccionesDAO;
import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.InventoryDTO;
import com.servipunto.micajaplus.database.dto.InventoryHistoryDTO;
import com.servipunto.micajaplus.database.dto.ProductDTO;
import com.servipunto.micajaplus.database.dto.SalesDTO;
import com.servipunto.micajaplus.database.dto.SalesDetailDTO;
import com.servipunto.micajaplus.database.dto.UserDetailsDTO;

import java.util.ArrayList;
import java.util.List;

public final class UpdateSynkStatus implements DTO {

	private List<DTO> lIst = new ArrayList<DTO>();
	private int tAbleType;

	public UpdateSynkStatus(List<DTO> list, int TableType) {
		this.lIst = list;
		this.tAbleType = TableType;
		try {
			updateLocalTable();
		} catch (Exception e) {
//			Fabric.getLogger().w();
			e.printStackTrace();
		}
	}

	private void updateLocalTable() throws Exception{

		if (tAbleType == ServiApplication.inventory) {
			InventoryUpdate();
		}else if (tAbleType == ServiApplication.inventoryHistory) {
			InventoryHistoryUpdate();
		}else if (tAbleType == ServiApplication.sales) {
			salesUpdate();
		}else if (tAbleType == ServiApplication.salesDetails) {
			salesDetailsUpdate();
		}else if (tAbleType == ServiApplication.userTable) {
			userTableUpdate();
		}else if (tAbleType == ServiApplication.updateProducts) {
			updateProductTableUpdate();
		}else if (tAbleType == ServiApplication.updatepuntoredsync) {
			updatepuntoredsync();
		}

	}

	private void updatepuntoredsync() {
		for (DTO dto : lIst) {
			SincronizarTransaccionesDAO.getInstance().updateServiTiendaSynck(DBHandler.getDBObj(Constants.WRITABLE), dto);
		}
		
	}

	private void updateProductTableUpdate() {
		for (DTO dto : lIst) {
			ProductDTO clientdto = (ProductDTO) dto;
			updateSyncStatus(DBHandler.getDBObj(Constants.WRITABLE),clientdto,"tblm_product","barcode",clientdto.getBarcode());
		}
	}

	private void userTableUpdate() {

		for (DTO dto : lIst) {
			UserDetailsDTO clientdto = (UserDetailsDTO) dto;
			updateSyncStatus(DBHandler.getDBObj(Constants.WRITABLE),clientdto,"tblm_user","user_id",clientdto.getUserId());
		}
	
		
	}


	private void salesDetailsUpdate() {
		for (DTO dto : lIst) {
			SalesDetailDTO clientdto = (SalesDetailDTO) dto;
			updateSyncStatus(DBHandler.getDBObj(Constants.WRITABLE),clientdto,"tbl_sales_details","sale_id",clientdto.getSaleId());
		}
	}

	private void salesUpdate() {

		for (DTO dto : lIst) {
			SalesDTO clientdto = (SalesDTO) dto;
			updateSyncStatus(DBHandler.getDBObj(Constants.WRITABLE),clientdto,"tbl_sales","sale_id",clientdto.getSaleID());
		}
	
	}


	private void InventoryHistoryUpdate() {

		for (DTO dto : lIst) {
			InventoryHistoryDTO clientdto = (InventoryHistoryDTO) dto;
			updateSyncStatus(DBHandler.getDBObj(Constants.WRITABLE),clientdto,"tbl_inventory_history","inventory_history_id",clientdto.getInventorHistoryId());
		}
		
	
		
	}

	private void InventoryUpdate() {

		for (DTO dto : lIst) {
			InventoryDTO clientdto = (InventoryDTO) dto;
			clientdto.setSyncStatus(1);
			updateDishSynkStatus(DBHandler.getDBObj(Constants.WRITABLE),clientdto);
		}

	}

	private void updateDishSynkStatus(SQLiteDatabase dbObject,
			InventoryDTO clientdto) {
		try {
			InventoryDTO dtoObj = clientdto;
			String whereCls = "inventory_id = '" + dtoObj.getInventoryId()
					+ "'";
			ContentValues cValues = new ContentValues();
			cValues.put("sync_status", dtoObj.getSyncStatus());
			dbObject.update("tbl_inventory", cValues, whereCls, null);
		} catch (SQLException e) {
			Log.e("clientDAO  -- update Debt", e.getMessage());
		} finally {
			dbObject.close();
		}
	}


	public boolean updateSyncStatus(SQLiteDatabase dbObject, DTO dto,String tableName,String primaryKey,String primarykeyvalue) {
		try {
			String whereCls = primaryKey+"="+"'" + primarykeyvalue + "'";
			ContentValues cValues = new ContentValues();
			cValues.put("sync_status", 1);
			dbObject.update(tableName, cValues, whereCls, null);
			return true;
		} catch (SQLException e) {
			Log.e("Update syntax  -- update Debt", e.getMessage());
		} finally {
			dbObject.close();
		}
		return true;
	}
}

package com.servipunto.micajaplus.utils;

/**
 * Created by Juan on 14/05/2018.
 */

public class Constants {

    public static final int   TRUE				     			= 1;
    public static final int   FALSE				     			= 0;
    public static final int READABLE							= 0;
    public static final int WRITABLE							= 1;
    public static String MyPREFERENCES = "com.servipunto.micajaplus.sharedpreferences";
    public static String URL = "http://juan9-dot-servi-test-saphety.appspot.com";
    public static final int MAX_OBJECTS=30;

}

package com.servipunto.micajaplus.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.servipunto.micajaplus.database.DBHandler;
import com.servipunto.micajaplus.database.dao.ProductDAO;
import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.InventoryDTO;
import com.servipunto.micajaplus.database.dto.InventoryHistoryDTO;
import com.servipunto.micajaplus.database.dto.ProductDTO;
import com.servipunto.micajaplus.database.dto.SalesDTO;
import com.servipunto.micajaplus.database.dto.SalesDetailDTO;
import com.servipunto.micajaplus.database.dto.TransaccionBoxDTO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class RequestFiels {
	SharedPreferences sharedpreferences;
	public RequestFiels(SharedPreferences sharedpreferences) {
		this.sharedpreferences = sharedpreferences;
	}

	public JSONObject TransaccionBox(List<DTO> TransaccionBoxDetails) {

		JSONObject jsonobj = new JSONObject();
		try {
			jsonobj.put("name", "transaction_box");
			jsonobj.put("type", "insert");
			JSONArray jsonRecordsArray = new JSONArray();
			for (int i = 0; i < TransaccionBoxDetails.size(); i++) {
				TransaccionBoxDTO clientdto = (TransaccionBoxDTO) TransaccionBoxDetails
						.get(i);
				JSONObject jso_client_obj = new JSONObject();
				jso_client_obj.put("store_code", clientdto.getStore_code());
				jso_client_obj.put("username", sharedpreferences.getString("username",""));
				jso_client_obj.put("tipo_transction",
						clientdto.getTipo_transction());
				jso_client_obj.put("module_name", clientdto.getModule_name());
				jso_client_obj.put("transaction_type",
						clientdto.getTransaction_type());
				jso_client_obj.put("amount", clientdto.getAmount());
				try {
					jso_client_obj.put("datetime",
							Dates.serverdateformate(clientdto.getDatetime()));
				} catch (Exception e) {
				}
				jsonRecordsArray.put(jso_client_obj);
			}
			jsonobj.put("records", jsonRecordsArray);
			System.out.println("Log: /sync: TransaccionBox"+ jsonobj.toString());

			return jsonobj;
		} catch (Exception e) {
			return jsonobj;
		}

	}
	public JSONObject getSalesData(List<DTO> sales) {

		JSONObject jsonobj = new JSONObject();
		try {
			jsonobj.put("name", "sales");
			jsonobj.put("type", "update");
			JSONArray columnarray = new JSONArray();
			columnarray.put("sale_id");
			columnarray.put("store_code");
			jsonobj.put("columns", columnarray);
			JSONArray jsonRecordsArray = new JSONArray();
			for (int i = 0; i < sales.size(); i++) {
				SalesDTO clientdto = (SalesDTO) sales.get(i);

				JSONObject sales1 = new JSONObject();

				JSONObject jso_client_obj = new JSONObject();
				jso_client_obj.put("sale_id", clientdto.getSaleID());

				jso_client_obj.put("invoice_number",
						clientdto.getInvoiceNumber());
				jso_client_obj.put("gross_amount",
						Double.parseDouble(clientdto.getGrossAmount()));
				jso_client_obj.put("net_amount",
						Double.parseDouble(clientdto.getNetAmount()));
				jso_client_obj.put("discount", clientdto.getDiscount());
				try {
					jso_client_obj
							.put("customer_code", clientdto.getClientId());
				} catch (Exception e) {

				}
				try {
					jso_client_obj.put("fecha_inicial", new Dates()
							.serverdateformate(clientdto.getFecha_inicial()));
				} catch (Exception e) {
					jso_client_obj.put("fecha_inicial", new Dates()
							.serverdateformate(Dates
									.getSysDate(Dates.YYYY_MM_DD_HH_MM)));
				}
				try {
					jso_client_obj.put("fecha_final", new Dates()
							.serverdateformate(clientdto.getFecha_final()));
				} catch (Exception e) {
					jso_client_obj.put("fecha_final", new Dates()
							.serverdateformate(Dates
									.getSysDate(Dates.YYYY_MM_DD_HH_MM)));
				}
				jso_client_obj.put("payment_type", clientdto.getPaymentType());
				jso_client_obj.put("modified_by", sharedpreferences.getString("username",""));
				jso_client_obj.put("amount_paid",
						Double.parseDouble(clientdto.getAmountPaid()));
				try {
					jso_client_obj.put("date_time",
							Dates.datetostring(clientdto.getDateTime()));
				} catch (Exception e) {

				}
				jso_client_obj.put("store_code", sharedpreferences.getString("store_code",""));

				sales1.put("sales", jso_client_obj);

//				sales1.put("sales_details",
//						getSales_details(clientdto.getSaleID()));
				sales1.put("sales_details","");
				if (sharedpreferences.getString("store_code","").length() >= 1
						&& sharedpreferences.getString("username","").length() >= 1) {
					jsonRecordsArray.put(sales1);
				}

			}
			jsonobj.put("records", jsonRecordsArray);
			System.out.println("Log: /sync: getSalesData"+ jsonobj.toString());

			return jsonobj;
		} catch (Exception e) {
			return jsonobj;
		}
	}
	public JSONObject getSalesDetailsData(List<DTO> salesDetails) {

		JSONObject jsonobj = new JSONObject();
		try {
			jsonobj.put("name", "sales_details");
			jsonobj.put("type", "insert");
			// JSONArray columnarray = new JSONArray();
			// columnarray.put("adjustment_id");
			// jsonobj.put("columns", columnarray);
			JSONArray jsonRecordsArray = new JSONArray();
			for (int i = 0; i < salesDetails.size(); i++) {
				SalesDetailDTO clientdto = (SalesDetailDTO) salesDetails.get(i);
				JSONObject jso_client_obj = new JSONObject();
				jso_client_obj.put("sale_id", clientdto.getSaleId());
				jso_client_obj.put("barcode", clientdto.getProductId());
				jso_client_obj.put("count", clientdto.getCount());
				jso_client_obj.put("price",
						Double.parseDouble(clientdto.getPrice()));

				try {
					jso_client_obj
							.put("purchase_price",
									ProductDAO
											.getInstance()
											.getRecordsByBarcode(
													DBHandler
															.getDBObj(Constants.READABLE),
													clientdto.getProductId())
											.getPurchasePrice());
				} catch (Exception e) {
				}
				jso_client_obj.put("uom", clientdto.getUom());
				try {
					jso_client_obj.put("dish_id",
							Long.parseLong(clientdto.getDishId()));
					jso_client_obj
							.put("purchase_price","");

				} catch (Exception e) {
				}
				jso_client_obj.put("store_code", sharedpreferences.getString("store_code",""));
				jso_client_obj.put("created_by",sharedpreferences.getString("username",""));

				if (sharedpreferences.getString("store_code","").length() >= 1
						&& sharedpreferences.getString("username","").length() >= 1) {
					jsonRecordsArray.put(jso_client_obj);
				}

			}
			jsonobj.put("records", jsonRecordsArray);
			System.out.println("Log: /sync: getSalesDetailsData"+ jsonobj.toString());

			return jsonobj;
		} catch (Exception e) {
			return jsonobj;
		}
	}
	public HashMap<String,Double> data (String data){
		try{
			JSONObject jsonobj = new JSONObject(data);
			JSONArray jsonArray = jsonobj.getJSONArray("data");
			HashMap<String, Double> pairs = new HashMap<String, Double>();
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject a= (JSONObject) jsonArray.get(i);
				String barcode = a.get("barcode").toString();
				double quantity = Double.parseDouble(String.valueOf(a.get("quantity")));
				pairs.put(barcode, quantity);
			}
			return pairs;

		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	public JSONObject getInventoryHistoryTableData(List<DTO> clientList) {

		JSONObject jsonobj = new JSONObject();
		try {
			jsonobj.put("name", "inventory_history");
			jsonobj.put("type", "insert");
			// JSONArray columnarray = new JSONArray();
			// columnarray.put("adjustment_id");
			// jsonobj.put("columns", columnarray);
			JSONArray jsonRecordsArray = new JSONArray();
			for (int i = 0; i < clientList.size(); i++) {
				InventoryHistoryDTO clientdto = (InventoryHistoryDTO) clientList
						.get(i);
				JSONObject jso_client_obj = new JSONObject();
				jso_client_obj.put("barcode", clientdto.getProductId());
				jso_client_obj.put("quantity", clientdto.getQuantity());
				try {
					jso_client_obj.put("date_time",Dates.serverdateformate(clientdto.getDateTime()));
				} catch (Exception e) {

				}
				jso_client_obj.put("created_by", sharedpreferences.getString("username",""));

				jso_client_obj.put("invoice_number", clientdto.getInvoiceNum());

				jso_client_obj.put("uom", clientdto.getUom());
				jso_client_obj.put("type_trans", clientdto.getTypeTrans());
				jso_client_obj.put("store_code", sharedpreferences.getString("store_code",""));
				if (sharedpreferences.getString("store_code","").length() >= 1
						&&sharedpreferences.getString("username","").length() >= 1) {
					jsonRecordsArray.put(jso_client_obj);
				}
			}
			jsonobj.put("records", jsonRecordsArray);
			System.out.println("Log: /sync: getInventoryHistoryTableData"+ jsonobj.toString());

			return jsonobj;
		} catch (Exception e) {
			return jsonobj;
		}

	}
	public JSONObject getInventoryTableData(List<DTO> clientPayList) {
		JSONObject jsonobj = new JSONObject();
		try {
			jsonobj.put("name", "inventory");
			jsonobj.put("type", "update");
			JSONArray columnarray = new JSONArray();
			columnarray.put("barcode");
			columnarray.put("store_code");
			jsonobj.put("columns", columnarray);
			JSONArray jsonRecordsArray = new JSONArray();
			for (int i = 0; i < clientPayList.size(); i++) {
				InventoryDTO clientdto = (InventoryDTO) clientPayList.get(i);
				JSONObject jso_client_obj = new JSONObject();
				jso_client_obj.put("inventory_id",
						Long.parseLong(clientdto.getInventoryId()));
				jso_client_obj.put("barcode", clientdto.getProductId());
				jso_client_obj.put("quantity", clientdto.getQuantityBalance().toString());
				jso_client_obj.put("store_code", sharedpreferences.getString("store_code",""));
				jso_client_obj.put("uom", clientdto.getUom());
				jso_client_obj.put("modified_by",sharedpreferences.getString("username",""));

				if (sharedpreferences.getString("store_code","").length() >= 1
						&& sharedpreferences.getString("username","").length() >= 1) {
					jsonRecordsArray.put(jso_client_obj);
				}

			}
			jsonobj.put("records", jsonRecordsArray);
			System.out.println("Log: /sync: getInventoryTableData"+ jsonobj.toString());

			return jsonobj;
		} catch (Exception e) {
			return jsonobj;
		}

	}
	public JSONObject getProductTableData(List<DTO> productDetails) {

		JSONObject jsonobj = new JSONObject();
		try {
			jsonobj.put("name", "product");
			jsonobj.put("type", "update");
			JSONArray columnarray = new JSONArray();
			columnarray.put("barcode");
			columnarray.put("store_code");
			jsonobj.put("columns", columnarray);
			JSONArray jsonRecordsArray = new JSONArray();
			for (int i = 0; i < productDetails.size(); i++) {
				ProductDTO clientdto = (ProductDTO) productDetails.get(i);
				JSONObject jso_client_obj = new JSONObject();
				jso_client_obj.put("barcode", clientdto.getBarcode());
				jso_client_obj.put("name", clientdto.getName());
				jso_client_obj.put("quantity", clientdto.getQuantity());
				jso_client_obj.put("uom", clientdto.getUom());
				jso_client_obj.put("purchase_price",
						Double.parseDouble(clientdto.getPurchasePrice()));
				jso_client_obj.put("selling_price",
						Double.parseDouble(clientdto.getSellingPrice()));
				jso_client_obj.put("group_id", clientdto.getGroupId());
				jso_client_obj.put("supplier_code", clientdto.getSupplierId());
				jso_client_obj.put("line_id", clientdto.getLineId());
				jso_client_obj.put("modified_by", sharedpreferences.getString("username",""));
				jso_client_obj.put("active_status",
						"" + clientdto.getActiveStatus());
				jso_client_obj.put("active_status",
						"" + clientdto.getActiveStatus());
				try {
					jso_client_obj
							.put("expiry_date", Dates
									.serverdateformate(clientdto
											.getExpiry_date()));
				} catch (Exception e) {

				}
				try {
					jso_client_obj.put("create_date",
							Dates.serverdateformate(clientdto.getCreateDate()));
					jso_client_obj.put("vat", clientdto.getVat());

				} catch (Exception e) {
				}
				try {
					jso_client_obj.put("modified_date", Dates
							.serverdateformate(clientdto.getModifiedDate()));
				} catch (Exception e) {

				}
				try {
					jso_client_obj.put("discount",
							Integer.parseInt(clientdto.getDiscount()));
				} catch (Exception e) {
					jso_client_obj.put("discount", 0);
				}
				// jso_client_obj.put("desc",""+clientdto.getDe);
				jso_client_obj.put("min_count_inventory",
						clientdto.getMin_count_inventory());
				jso_client_obj.put("store_code", sharedpreferences.getString("store_code",""));
				jso_client_obj.put("sync_status", clientdto.getSyncStatus());
				try {
					jso_client_obj.put("fecha_inicial", new Dates()
							.serverdateformate(clientdto.getFecha_inicial()));
				} catch (Exception e) {
					jso_client_obj.put("fecha_inicial", new Dates()
							.serverdateformate(Dates
									.getSysDate(Dates.YYYY_MM_DD_HH_MM)));
				}
				try {
					jso_client_obj.put("fecha_final", new Dates()
							.serverdateformate(clientdto.getFecha_final()));
				} catch (Exception e) {
					jso_client_obj.put("fecha_final", new Dates()
							.serverdateformate(Dates
									.getSysDate(Dates.YYYY_MM_DD_HH_MM)));
				}

				if (sharedpreferences.getString("store_code","").length() >= 1
						&& sharedpreferences.getString("username","").length() >= 1) {
					jsonRecordsArray.put(jso_client_obj);
				}
			}
			jsonobj.put("records", jsonRecordsArray);
			System.out.println("Log: /sync: getProductTableData"+ jsonobj.toString());

			return jsonobj;
		} catch (Exception e) {
			return jsonobj;
		}
	}

}

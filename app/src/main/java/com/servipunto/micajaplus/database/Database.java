package com.servipunto.micajaplus.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {

        private final static String DATABASE_NAME = "MiCajaPlus.db";
        private final static int DATABASE_VERSION = 12;

        private final String USER_DETAILS = "CREATE TABLE tblm_user (user_id TEXT,nit_shop_id TEXT,user_name TEXT  primary key,password TEXT,name TEXT,company_name TEXT,imei TEXT,registration_date TEXT,last_login TEXT,cedula_document TEXT,opening_balance TEXT,opening_date_time TEXT,is_closed TEXT,close_date_time TEXT,sync_status TEXT,actual_balance TEXT,terminal_Id TEXT,puntored_Id TEXT,comercio_Id TEXT,system_Id TEXT,is_admin TEXT,is_authorized TEXT,email TEXT ,authtoken TEXT)";
        private final String USER_MODULEID = "Create table UserModules(userModuleId integer primary key autoincrement, moduleName text, moduleCode text)";
        private final String INVENTORY = "CREATE TABLE tbl_inventory(inventory_id TEXT primary key, product_id text, quantity text, uom text, sync_status integer, quantity_balance REAL)";
        private final String PRODUCT = "CREATE TABLE tblm_product (product_id TEXT primary key,barcode TEXT,name TEXT,quantity TEXT,uom TEXT,purchase_price TEXT,selling_price TEXT,group_id TEXT,vat TEXT,supplier_id TEXT,line_id TEXT,active_status TEXT,create_date TEXT,modified_date TEXT,productFlag TEXT,sync_status TEXT,sub_group TEXT,min_count_inventory TEXT,expiry_date TEXT,discount TEXT,fecha_inicial TEXT,fecha_final TEXT,favorite TEXT)";
        private final String INVENTORY_HISTORY = "CREATE TABLE tbl_inventory_history (inventory_history_id TEXT primary key,product_id TEXT,quantity TEXT,uom TEXT,price integer,date_time TEXT,sync_status TEXT,invoice_no TEXT, type_trans TEXT)";
        private final String TRANSACCION_BOX = "Create table tbl_transaccion_box(transaccion_box_id text, store_code text, user_name text,tipo_trans text,module_name text,transaccion_type text,amount text,date_time text,sync_status integer)";
        private final String SALES = "CREATE TABLE tbl_sales(sale_id text primary key,invoice_number text,gross_amount text,net_amount text,discount text,client_id text,amount_paid text,payment_type text,date_time text,sync_status integer, fecha_inicial TEXT, fecha_final TEXT)";
        private final String SALES_DETAILS = "CREATE TABLE tbl_sales_details(sales_details_id  integer primary key,sale_id text,product_id text,count text,uom text,price text,dish_id text,sync_status integer)";
        private final String SINCRONIZAR_TRANSACCIONES = "Create table tbl_sincronizar_transaccion(rowid text , module text, tipo_transaction text,authorization_number text,id_pdb_servitienda text,transaction_datetime text,transaction_value text,status text,creation_date text,created_by text,modified_date text,modified_by text,servitienda_synck_status text, punthored_synck_status text, module_tipo_id text)";

    public Database(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase dbObj) {
            dbObj.execSQL(USER_DETAILS);
            dbObj.execSQL(USER_MODULEID);
            dbObj.execSQL(INVENTORY);
            dbObj.execSQL(PRODUCT);
            dbObj.execSQL(INVENTORY_HISTORY);
            dbObj.execSQL(TRANSACCION_BOX);
            dbObj.execSQL(SALES);
            dbObj.execSQL(SALES_DETAILS);
            dbObj.execSQL(SINCRONIZAR_TRANSACCIONES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            System.out.println("oldVersion :" + oldVersion);
            System.out.println("newVersion :" + newVersion);
            if (oldVersion < newVersion) {
                switch (newVersion) {
                    case 6:
                        alterUser(db);
                        alterInventory(db);
                        alterProduct(db);
                        db.execSQL("ALTER TABLE tblm_product ADD COLUMN favorite TEXT");
                        db.execSQL("ALTER TABLE tbl_inventory_history ADD COLUMN type_trans TEXT");
                        alterSales(db);
                        break;
                    default:
                        break;
                }
            }
        }

        private void alterUser(SQLiteDatabase db) {
            db.execSQL("ALTER TABLE tblm_user RENAME TO tmp");
            db.execSQL("CREATE TABLE tblm_user(user_id TEXT,nit_shop_id TEXT,user_name TEXT  primary key,password TEXT,name TEXT,company_name TEXT,imei TEXT,registration_date TEXT,last_login TEXT,cedula_document TEXT,opening_balance TEXT,opening_date_time TEXT,is_closed TEXT,close_date_time TEXT,sync_status TEXT,actual_balance TEXT,terminal_Id TEXT,puntored_Id TEXT,comercio_Id TEXT,system_Id TEXT,is_admin TEXT,is_authorized TEXT,email TEXT ,authtoken TEXT)");
            db.execSQL("INSERT INTO tblm_user(user_id,nit_shop_id,user_name,password,name,company_name,imei,registration_date,last_login,cedula_document,opening_balance,opening_date_time,is_closed,close_date_time,sync_status,actual_balance,is_admin,is_authorized,authtoken)  SELECT user_id,nit_shop_id,user_name,password,name,company_name,imei,registration_date,last_login,cedula_document,opening_balance,opening_date_time,is_closed,close_date_time,sync_status,actual_balance,is_admin,is_authorized,authtoken FROM tmp");
            db.execSQL("DROP TABLE tmp");
        }
    private void alterInventory(SQLiteDatabase db) {
        System.out.println("Inside alter inventory table");
        db.execSQL("ALTER TABLE tbl_inventory RENAME TO tmp");
        db.execSQL("CREATE TABLE tbl_inventory(inventory_id TEXT primary key, product_id text, quantity text, uom text, sync_status integer)");

        db.execSQL("INSERT INTO tbl_inventory(inventory_id,product_id,quantity,uom,sync_status)  SELECT inventory_id,product_id,quantity,uom,sync_status  FROM tmp");
        db.execSQL("DROP TABLE tmp");
    }
    private void alterProduct(SQLiteDatabase db) {

        db.execSQL("ALTER TABLE tblm_product RENAME TO tmp");
        db.execSQL("CREATE TABLE tblm_product(product_id TEXT primary key,barcode TEXT,name TEXT,quantity TEXT,uom TEXT,purchase_price TEXT,selling_price TEXT,group_id TEXT,vat TEXT,supplier_id TEXT,line_id TEXT,active_status TEXT,create_date TEXT,modified_date TEXT,productFlag TEXT,sync_status TEXT,sub_group TEXT,min_count_inventory TEXT,expiry_date TEXT,discount TEXT,fecha_inicial TEXT,fecha_final TEXT)");
        db.execSQL("INSERT INTO tblm_product(product_id,barcode,name,quantity,uom,purchase_price,selling_price,group_id,vat,supplier_id,line_id,active_status,create_date,modified_date,productFlag,sync_status)  SELECT product_id,barcode,name,quantity,uom,purchase_price,selling_price,group_id,vat,supplier_id,line_id,active_status,create_date,modified_date,productFlag,sync_status FROM tmp");
        db.execSQL("DROP TABLE tmp");
    }
    private void alterSales(SQLiteDatabase db) {

        db.execSQL("ALTER TABLE tbl_sales RENAME TO tmp");
        db.execSQL("CREATE TABLE tbl_sales(sale_id text primary key,invoice_number text,gross_amount text,net_amount text,discount text,client_id text,amount_paid text,payment_type text,date_time text,sync_status integer, fecha_inicial text, fecha_final text)");
        db.execSQL("INSERT INTO tbl_sales(sale_id,invoice_number,gross_amount,net_amount,discount,client_id,amount_paid,payment_type,date_time,sync_status)  SELECT sale_id,invoice_number,gross_amount,net_amount,discount,client_id,amount_paid,payment_type,date_time,sync_status FROM tmp");
        db.execSQL("DROP TABLE tmp");
    }
    }

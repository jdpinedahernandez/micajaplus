/*******************************************************************************
 *  @author 
 *  Ybrant Digital
 *  Copyright (C) Ybrant Digital
 *  http://www.ybrantdigital.com
 *******************************************************************************/
package com.servipunto.micajaplus.database.dto;

public class SelectedProddutsDTO implements DTO {
	private String idProduct;
	private String barcode;
	private String name;
	private String quantity;
	private String units;
	private String price;
	private String vat;
	private String actualQty;
	private String sellPrice;
	private String modifiedDate;
	private String idDishProduct;
	private String utilityValue;
	private String productType;
	private String saleID;
	private String inventoryType;
	private String expiry_date;

	public SelectedProddutsDTO(DTO dtoToCopy) {
        SelectedProddutsDTO dto = (SelectedProddutsDTO) dtoToCopy;
		idProduct = dto.getIdProduct();
		barcode = dto.getBarcode();
		name = dto.getName();
		quantity = dto.getQuantity();
		units = dto.getUnits();
		price = dto.getPrice();
		vat = dto.getVat();
		actualQty =  dto.getQuantity();
		sellPrice = dto.getSellPrice();
		modifiedDate = dto.getModifiedDate();
		idDishProduct = dto.getIdDishProduct();
		utilityValue = dto.getUtilityValue();
		productType = dto.getProductType();
		saleID = dto.getSaleID();
		inventoryType = dto.getInventoryType();
		expiry_date = dto.getExpiry_date();
	}


	public SelectedProddutsDTO(){}


	public void setInventoryData (String idProduct, String barcode, String name, String quantity, String uom,
								  String sellPrice, String price, String vat, String productType){

		this.idProduct = idProduct;
		this.barcode = barcode;
		this.name = name;
		this.quantity = quantity;
		this.units = uom;
		this.sellPrice = sellPrice;
		this.price = price;
		this.vat = vat;
		this.productType = productType;

	}
	public String getExpiry_date() {
		return expiry_date;
	}

	public void setExpiry_date(String expiry_date) {
		this.expiry_date = expiry_date;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getUtilityValue() {
		return utilityValue;
	}

	public void setUtilityValue(String utilityValue) {
		this.utilityValue = utilityValue;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getActualQty() {
		return actualQty;
	}

	public void setActualQty(String actualQty) {
		this.actualQty = actualQty;
	}

	public String getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(String sellPrice) {
		this.sellPrice = sellPrice;
	}

	public String getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getUnits() {
		return units;
	}

	public void setUnits(String units) {
		this.units = units;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getVat() {
		return vat;
	}

	public void setVat(String vat) {
		this.vat = vat;
	}

	public String getIdDishProduct() {
		return idDishProduct;
	}

	public void setIdDishProduct(String idDishProduct) {
		this.idDishProduct = idDishProduct;
	}

	public String getSaleID() {
		return saleID;
	}

	public void setSaleID(String saleID) {
		this.saleID = saleID;
	}

	public String getInventoryType() {
		return inventoryType;
	}

	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
	}

}

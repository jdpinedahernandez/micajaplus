/*******************************************************************************
 *  @author
 *  Ybrant Digital
 *  Copyright (C) Ybrant Digital
 *  http://www.ybrantdigital.com
 *******************************************************************************/
package com.servipunto.micajaplus.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.ProductDTO;
import com.servipunto.micajaplus.database.dto.SelectedProddutsDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProductDAO implements DAO {

	private static ProductDAO productDAO;

	private ProductDAO() {

	}

	public static ProductDAO getInstance() {
		if (productDAO == null)
			productDAO = new ProductDAO();

		return productDAO;
	}

	public boolean deleteAllRecords(SQLiteDatabase dbObj) {
		try {
			dbObj.compileStatement("DELETE  FROM tblm_product").execute();

			return true;
		} catch (Exception e) {
			Log.e("tblm_product  -- delete", e.getMessage());
		}

		finally {
			dbObj.close();
		}
		return false;
	}
	public int Count(SQLiteDatabase dbObj) {
		Cursor cursor = null;
		int count = 0, num = 0;
		try {
			String countQuery = "SELECT COUNT(*) FROM tblm_product WHERE active_status = 1";
			cursor = dbObj.rawQuery(countQuery, null);
			if (cursor.moveToFirst()) {
				num = cursor.getInt(count++);
			}
			return num;
		} catch (Exception e) {
			Log.e("ProductDAO  -- Count", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}
		return num;
	}
	public int Count_total(SQLiteDatabase dbObj) {
		Cursor cursor = null;
		int count = 0, num = 0;
		try {
			String countQuery = "SELECT COUNT(*) FROM tblm_product";
			cursor = dbObj.rawQuery(countQuery, null);
			if (cursor.moveToFirst()) {
				num = cursor.getInt(count++);
			}
			return num;
		} catch (Exception e) {
			Log.e("ProductDAO  -- Count", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}
		return num;
	}
	@Override
	public boolean insert(SQLiteDatabase dbObj, List<DTO> list) {
		try {
			dbObj.beginTransaction();
			SQLiteStatement stmt = dbObj.compileStatement(
					"INSERT INTO tblm_product(product_id,barcode,name,quantity,uom,purchase_price,selling_price,group_id,vat,supplier_id,line_id,active_status,create_date,modified_date,productFlag,sync_status,sub_group,min_count_inventory,expiry_date,discount,fecha_inicial,fecha_final,favorite)VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			int count = 1;

			for (DTO items : list) {
				ProductDTO dto = (ProductDTO) items;
				stmt.bindString(count++, UUID.randomUUID().toString());
				// stmt.bindString(count++, dto.getProductId());
				stmt.bindString(count++, dto.getBarcode() == null ? "" : dto.getBarcode());
				stmt.bindString(count++, dto.getName() == null ? "" : dto.getName());
				stmt.bindString(count++, dto.getQuantity() == null ? "" : dto.getQuantity());
				stmt.bindString(count++, dto.getUom() == null ? "" : dto.getUom());
				stmt.bindString(count++, dto.getPurchasePrice() == null ? "" : dto.getPurchasePrice());
				stmt.bindString(count++, dto.getSellingPrice() == null ? "" : dto.getSellingPrice());
				stmt.bindString(count++, dto.getGroupId() == null ? "" : dto.getGroupId());
				stmt.bindString(count++, dto.getVat() == null ? "" : dto.getVat());
				stmt.bindString(count++, dto.getSupplierId() == null ? "" : dto.getSupplierId());
				stmt.bindString(count++, dto.getLineId() == null ? "" : dto.getLineId());
				stmt.bindLong(count++, dto.getActiveStatus() == null ? 1 : dto.getActiveStatus());
				stmt.bindString(count++, dto.getCreateDate() == null ? "" : dto.getCreateDate());
				stmt.bindString(count++, dto.getModifiedDate() == null ? "" : dto.getModifiedDate());
				stmt.bindString(count++, dto.getProductFlag() == null ? "" : dto.getProductFlag());
				stmt.bindLong(count++, dto.getSyncStatus() == null ? 0 : dto.getSyncStatus());
				stmt.bindString(count++, dto.getSubgroup() == null ? "" : dto.getSubgroup());
				stmt.bindString(count++, dto.getMin_count_inventory() == null ? "" : dto.getMin_count_inventory());
				stmt.bindString(count++, dto.getExpiry_date() == null ? "" : dto.getExpiry_date());
				stmt.bindString(count++, dto.getDiscount() == null ? "" : dto.getDiscount());
				stmt.bindString(count++, dto.getFecha_inicial() == null ? "" : dto.getFecha_inicial());
				stmt.bindString(count++, dto.getFecha_final() == null ? "" : dto.getFecha_final());
				stmt.bindString(count++, dto.getFavorite() == null ? "" : dto.getFavorite());
				count = 1;

				stmt.executeInsert();
			}

			dbObj.setTransactionSuccessful();
			return true;
		} catch (Exception e) {
			Log.e("ProductDAO  -- insert", e.getMessage());
		} finally {
			dbObj.endTransaction();
			dbObj.close();
		}
		return false;
	}

	@Override
	public boolean update(SQLiteDatabase dbObject, DTO dto) {
		try {
			ProductDTO dtoObj = (ProductDTO) dto;

			String whereCls = "product_id = '" + dtoObj.getProductId() + "'";
			ContentValues cValues = new ContentValues();
			cValues.put("barcode", dtoObj.getBarcode());
			cValues.put("name", dtoObj.getName());
			cValues.put("quantity", dtoObj.getQuantity());
			cValues.put("UOM", dtoObj.getUom());
			cValues.put("purchase_price", dtoObj.getPurchasePrice());
			cValues.put("selling_price", dtoObj.getSellingPrice());
			cValues.put("group_id", dtoObj.getGroupId());
			cValues.put("vat", dtoObj.getVat());
			cValues.put("supplier_id", dtoObj.getSupplierId());
			cValues.put("line_id", dtoObj.getLineId());
			cValues.put("active_status", dtoObj.getActiveStatus());
			cValues.put("create_date", dtoObj.getCreateDate());
			cValues.put("modified_date", dtoObj.getModifiedDate());
			cValues.put("productFlag", dtoObj.getProductFlag());
			cValues.put("sync_status", dtoObj.getSyncStatus());
			cValues.put("sub_group", dtoObj.getSubgroup());
			cValues.put("min_count_inventory", dtoObj.getMin_count_inventory());
			cValues.put("expiry_date", dtoObj.getExpiry_date());
			cValues.put("discount", dtoObj.getDiscount());
			cValues.put("fecha_inicial", dtoObj.getFecha_inicial());
			cValues.put("fecha_final", dtoObj.getFecha_final());
			cValues.put("favorite", dtoObj.getFavorite());

			dbObject.update("tblm_product", cValues, whereCls, null);

			return true;
		} catch (SQLException e) {
			Log.e("ProductDAO  -- update", e.getMessage());
		} finally {
			dbObject.close();
		}
		return false;
	}

	public boolean updateByBarcode(SQLiteDatabase dbObject, DTO dto) {
		try {
			ProductDTO dtoObj = (ProductDTO) dto;

			String whereCls = "barcode = '" + dtoObj.getBarcode() + "'";
			ContentValues cValues = new ContentValues();
			cValues.put("barcode", dtoObj.getBarcode());
			cValues.put("name", dtoObj.getName());
			cValues.put("quantity", dtoObj.getQuantity());
			cValues.put("UOM", dtoObj.getUom());
			cValues.put("purchase_price", dtoObj.getPurchasePrice());
			cValues.put("selling_price", dtoObj.getSellingPrice());
			cValues.put("group_id", dtoObj.getGroupId());
			cValues.put("vat", dtoObj.getVat());
			cValues.put("supplier_id", dtoObj.getSupplierId());
			cValues.put("line_id", dtoObj.getLineId());
			cValues.put("active_status", dtoObj.getActiveStatus());
			cValues.put("create_date", dtoObj.getCreateDate());
			cValues.put("modified_date", dtoObj.getModifiedDate());
			cValues.put("productFlag", dtoObj.getProductFlag());
			cValues.put("sync_status", dtoObj.getSyncStatus());
			cValues.put("sub_group", dtoObj.getSubgroup());
			cValues.put("min_count_inventory", dtoObj.getMin_count_inventory());
			cValues.put("expiry_date", dtoObj.getExpiry_date());
			cValues.put("discount", dtoObj.getDiscount());
			cValues.put("fecha_inicial", dtoObj.getFecha_inicial());
			cValues.put("fecha_final", dtoObj.getFecha_final());

			dbObject.update("tblm_product", cValues, whereCls, null);

			return true;
		} catch (SQLException e) {
			Log.e("ProductDAO  -- update", e.getMessage());
		} finally {
			dbObject.close();
		}
		return false;
	}

	public boolean updateSynckStatusasone(SQLiteDatabase dbObject, String barcode) {
		try {
			// ProductDTO dtoObj = (ProductDTO) dto;
			String whereCls = "barcode = '" + barcode + "'";
			ContentValues cValues = new ContentValues();
			cValues.put("sync_status", 0);
			dbObject.update("tblm_product", cValues, whereCls, null);
			return true;
		} catch (SQLException e) {
			Log.e("ProductDAO  -- update Debt", e.getMessage());
		} finally {
			dbObject.close();
		}
		return false;
	}

	public boolean updateSynckDate(SQLiteDatabase dbObject, DTO dto) {
		try {
			ProductDTO dtoObj = (ProductDTO) dto;
			String whereCls = "barcode = '" + dtoObj.getBarcode() + "'";
			ContentValues cValues = new ContentValues();
			cValues.put("sync_status", 0);
			dbObject.update("tblm_product", cValues, whereCls, null);
			return true;
		} catch (SQLException e) {
			Log.e("ProductDAO  -- update Debt", e.getMessage());
		} finally {
			dbObject.close();
		}
		return false;
	}

	@Override
	public boolean delete(SQLiteDatabase dbObj, DTO dto) {
		ProductDTO productDTO = (ProductDTO) dto;
		try {
			dbObj.compileStatement("DELETE FROM PRODUCT WHERE tblm_product = '" + productDTO.getProductId() + "'")
					.execute();

			return true;
		} catch (Exception e) {
			Log.e("ProductDAO  -- delete", e.getMessage());
		}

		finally {
			dbObj.close();
		}
		return false;
	}

	@Override
	public List<DTO> getRecords(SQLiteDatabase dbObj) {
		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;

		int count = 0;

		try {
			cursor = dbObj.rawQuery("SELECT * FROM tblm_product WHERE active_status=1", null);

			if (cursor.moveToFirst()) {
				do {
					ProductDTO dto = new ProductDTO();
					dto.setProductId(cursor.getString(count++));
					dto.setBarcode(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setPurchasePrice(cursor.getString(count++));
					dto.setSellingPrice(cursor.getString(count++));
					dto.setGroupId(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setSupplierId(cursor.getString(count++));
					dto.setLineId(cursor.getString(count++));
					dto.setActiveStatus(cursor.getInt(count++));
					dto.setCreateDate(cursor.getString(count++));
					dto.setModifiedDate(cursor.getString(count++));
					dto.setProductFlag(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));
					dto.setSubgroup(cursor.getString(count++));
					dto.setMin_count_inventory(cursor.getString(count++));
					dto.setExpiry_date(cursor.getString(count++));
					dto.setDiscount(cursor.getString(count++));
					dto.setFecha_inicial(cursor.getString(count++));
					dto.setFecha_final(cursor.getString(count++));
					dto.setFavorite(cursor.getString(count++));
					instList.add(dto);

					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("ProductDAO  -- getRecords", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}

		return instList;
	}

	// INSERT INTO
	// tblm_product(product_id,barcode,name,quantity,uom,purchase_price,selling_price,group_id,vat,supplier_id,line_id,active_status,create_date,modified_date,sync_status)VALUES
	// (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); int count = 1;

	public ProductDTO getRecordsByProductID(SQLiteDatabase dbObj, String productCode) {

		ProductDTO dto = new ProductDTO();
		Cursor cursor = null;

		int count = 0;

		try {
			cursor = dbObj.rawQuery("SELECT * FROM tblm_product WHERE  barcode = '" + productCode + "'", null);
			if (cursor.moveToFirst()) {
				do {

					dto.setProductId(cursor.getString(count++));
					dto.setBarcode(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setPurchasePrice(cursor.getString(count++));
					dto.setSellingPrice(cursor.getString(count++));
					dto.setGroupId(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setSupplierId(cursor.getString(count++));
					dto.setLineId(cursor.getString(count++));
					dto.setActiveStatus(cursor.getInt(count++));
					dto.setCreateDate(cursor.getString(count++));
					dto.setModifiedDate(cursor.getString(count++));
					dto.setProductFlag(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));
					dto.setSubgroup(cursor.getString(count++));
					dto.setMin_count_inventory(cursor.getString(count++));
					dto.setExpiry_date(cursor.getString(count++));
					dto.setDiscount(cursor.getString(count++));
					dto.setFecha_inicial(cursor.getString(count++));
					dto.setFecha_final(cursor.getString(count++));
					dto.setFavorite(cursor.getString(count++));
					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("ProductDAO  -- getRecords", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}

		return dto;
	}

	public int isProductExists(SQLiteDatabase dbObj, String productCode) {

		ProductDTO dto = new ProductDTO();
		Cursor cursor = null;

		int count = 0;

		try {
			cursor = dbObj.rawQuery("SELECT * FROM tblm_product WHERE  barcode = '" + productCode + "'", null);

			count = cursor.getCount();
			return count;

		} catch (Exception e) {
			Log.e("ProductDAO  -- getRecords", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}

		return count;
	}

	public List<DTO> getProductBarcode(SQLiteDatabase dbObject, String Name) {

		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;
		int count = 0;
		try {
			cursor = dbObject.rawQuery("SELECT barcode FROM tblm_product WHERE name like '%" + Name + "%'",
					null);
			if (cursor.moveToFirst()) {
//				do {
				ProductDTO dto = new ProductDTO();
				dto.setBarcode(cursor.getString(count++));
				instList.add(dto);
				count = 0;
//				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("SupplierPaymentsDTO  -- getRecordsWithValues", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();
			dbObject.close();
		}

		return instList;

	}


	@Override
	public List<DTO> getRecordsWithValues(SQLiteDatabase dbObject, String columnName, String columnValue) {

		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;
		int count = 0;
		try {
			cursor = dbObject.rawQuery("SELECT * FROM tblm_product WHERE " + columnName + " = '" + columnValue + "'",
					null);
			if (cursor.moveToFirst()) {
				do {
					ProductDTO dto = new ProductDTO();
					dto.setProductId(cursor.getString(count++));
					dto.setBarcode(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setQuantity((cursor.getString(count++)));
					dto.setUom((cursor.getString(count++)));
					dto.setPurchasePrice((cursor.getString(count++)));
					dto.setSellingPrice(cursor.getString(count++));
					dto.setGroupId(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setSupplierId((cursor.getString(count++)));
					dto.setLineId((cursor.getString(count++)));
					dto.setActiveStatus((cursor.getInt(count++)));
					dto.setCreateDate((cursor.getString(count++)));
					dto.setModifiedDate((cursor.getString(count++)));
					dto.setProductFlag(cursor.getString(count++));
					dto.setSyncStatus((cursor.getInt(count++)));
					dto.setSubgroup((cursor.getString(count++)));
					dto.setMin_count_inventory(cursor.getString(count++));
					dto.setExpiry_date(cursor.getString(count++));
					dto.setDiscount(cursor.getString(count++));
					dto.setFecha_inicial(cursor.getString(count++));
					dto.setFecha_final(cursor.getString(count++));
					dto.setFavorite(cursor.getString(count++));
					instList.add(dto);
					count = 0;
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("SupplierPaymentsDTO  -- getRecordsWithValues", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();
			dbObject.close();
		}

		return instList;

	}

	public List<DTO> getRecordsBySupplierID(SQLiteDatabase dbObj, String supplierID) {

		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;
		int count = 0;
		try {
			cursor = dbObj.rawQuery(
					"SELECT * FROM tblm_product WHERE  supplier_id = '" + supplierID + "' order by UPPER(name) ASC",
					null);
			if (cursor.moveToFirst()) {
				do {
					ProductDTO dto = new ProductDTO();
					dto.setProductId(cursor.getString(count++));
					dto.setBarcode(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setPurchasePrice(cursor.getString(count++));
					dto.setSellingPrice(cursor.getString(count++));
					dto.setGroupId(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setSupplierId(cursor.getString(count++));
					dto.setLineId(cursor.getString(count++));
					dto.setActiveStatus(cursor.getInt(count++));
					dto.setCreateDate(cursor.getString(count++));
					dto.setModifiedDate(cursor.getString(count++));
					dto.setProductFlag(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));
					dto.setSubgroup(cursor.getString(count++));
					dto.setMin_count_inventory(cursor.getString(count++));
					dto.setExpiry_date(cursor.getString(count++));
					dto.setDiscount(cursor.getString(count++));
					dto.setFecha_inicial(cursor.getString(count++));
					dto.setFecha_final(cursor.getString(count++));
					dto.setFavorite(cursor.getString(count++));
					instList.add(dto);

					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("ProductDAO  -- getRecords", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}

		return instList;
	}
	public List<DTO> getOldRecords(SQLiteDatabase dbObj) {
		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;

		int count = 0;

		try {
			cursor = dbObj.rawQuery("SELECT product_id ,barcode,name,active_status,create_date,modified_date,sync_status FROM tblm_product where active_status = '0'", null); //

			if (cursor.moveToFirst()) {
				do {
					ProductDTO dto = new ProductDTO();
					dto.setProductId(cursor.getString(count++));
					dto.setBarcode(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setActiveStatus(cursor.getInt(count++));
					dto.setCreateDate(cursor.getString(count++));
					dto.setModifiedDate(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));
					instList.add(dto);

					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("ProductDAO  -- getRecords", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}

		return instList;
	}

	public ProductDTO getRecordsByBarcode(SQLiteDatabase dbObj, String barcode) {

		ProductDTO dto = new ProductDTO();
		Cursor cursor = null;

		int count = 0;

		try {
			cursor = dbObj.rawQuery("SELECT * FROM tblm_product WHERE  barcode = '" + barcode + "'", null);
			if (cursor.moveToFirst()) {
				do {

					dto.setProductId(cursor.getString(count++));
					dto.setBarcode(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setPurchasePrice(cursor.getString(count++));
					dto.setSellingPrice(cursor.getString(count++));
					dto.setGroupId(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setSupplierId(cursor.getString(count++));
					dto.setLineId(cursor.getString(count++));
					dto.setActiveStatus(cursor.getInt(count++));
					dto.setCreateDate(cursor.getString(count++));
					dto.setModifiedDate(cursor.getString(count++));
					dto.setProductFlag(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));
					dto.setSubgroup(cursor.getString(count++));
					dto.setMin_count_inventory(cursor.getString(count++));
					dto.setExpiry_date(cursor.getString(count++));
					dto.setDiscount(cursor.getString(count++));
					dto.setFecha_inicial(cursor.getString(count++));
					dto.setFecha_final(cursor.getString(count++));
					dto.setFavorite(cursor.getString(count++));
					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("ProductDAO  -- getRecords with bar code", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}

		return dto;
	}
	public List<DTO> getFavoriteProducts(SQLiteDatabase dbObj) {

		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;
		int count = 0;
		try {
			cursor = dbObj.rawQuery(
//					"SELECT * FROM tblm_product WHERE  favorite = '1' and active_status = 1",
					"select * from tblm_product where (favorite = 1 or favorite = 3) and active_status = 1",
					null);
			if (cursor.moveToFirst()) {
				do {
					ProductDTO dto = new ProductDTO();
					dto.setProductId(cursor.getString(count++));
					dto.setBarcode(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setPurchasePrice(cursor.getString(count++));
					dto.setSellingPrice(cursor.getString(count++));
					dto.setGroupId(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setSupplierId(cursor.getString(count++));
					dto.setLineId(cursor.getString(count++));
					dto.setActiveStatus(cursor.getInt(count++));
					dto.setCreateDate(cursor.getString(count++));
					dto.setModifiedDate(cursor.getString(count++));
					dto.setProductFlag(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));
					dto.setSubgroup(cursor.getString(count++));
					dto.setMin_count_inventory(cursor.getString(count++));
					dto.setExpiry_date(cursor.getString(count++));
					dto.setDiscount(cursor.getString(count++));
					dto.setFecha_inicial(cursor.getString(count++));
					dto.setFecha_final(cursor.getString(count++));
					dto.setFavorite(cursor.getString(count++));
					instList.add(dto);

					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("ProductDAO  -- getRecords", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}

		return instList;
	}

	public List<DTO> getCafeProducts(SQLiteDatabase dbObj) {

		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;
		int count = 0;
		try {
			cursor = dbObj.rawQuery(
					"select * from tblm_product where (favorite = 2 or favorite = 3) and active_status = 1",
					null);
			if (cursor.moveToFirst()) {
				do {
					ProductDTO dto = new ProductDTO();
					dto.setProductId(cursor.getString(count++));
					dto.setBarcode(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setPurchasePrice(cursor.getString(count++));
					dto.setSellingPrice(cursor.getString(count++));
					dto.setGroupId(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setSupplierId(cursor.getString(count++));
					dto.setLineId(cursor.getString(count++));
					dto.setActiveStatus(cursor.getInt(count++));
					dto.setCreateDate(cursor.getString(count++));
					dto.setModifiedDate(cursor.getString(count++));
					dto.setProductFlag(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));
					dto.setSubgroup(cursor.getString(count++));
					dto.setMin_count_inventory(cursor.getString(count++));
					dto.setExpiry_date(cursor.getString(count++));
					dto.setDiscount(cursor.getString(count++));
					dto.setFecha_inicial(cursor.getString(count++));
					dto.setFecha_final(cursor.getString(count++));
					dto.setFavorite(cursor.getString(count++));
					instList.add(dto);

					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("ProductDAO  -- getRecords", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}

		return instList;
	}

	public List<DTO> getUOMProduct(SQLiteDatabase dbObj) {

		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;
		int count = 0;
		try {
			cursor = dbObj.rawQuery(
					"SELECT * FROM tblm_product where (uom = 'kg' OR uom = 'gm' OR uom = 'gm' OR uom = 'lt' OR uom = 'ml') and active_status = 1",
					null);
			if (cursor.moveToFirst()) {
				do {
					ProductDTO dto = new ProductDTO();
					dto.setProductId(cursor.getString(count++));
					dto.setBarcode(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setPurchasePrice(cursor.getString(count++));
					dto.setSellingPrice(cursor.getString(count++));
					dto.setGroupId(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setSupplierId(cursor.getString(count++));
					dto.setLineId(cursor.getString(count++));
					dto.setActiveStatus(cursor.getInt(count++));
					dto.setCreateDate(cursor.getString(count++));
					dto.setModifiedDate(cursor.getString(count++));
					dto.setProductFlag(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));
					dto.setSubgroup(cursor.getString(count++));
					dto.setMin_count_inventory(cursor.getString(count++));
					dto.setExpiry_date(cursor.getString(count++));
					dto.setDiscount(cursor.getString(count++));
					dto.setFecha_inicial(cursor.getString(count++));
					dto.setFecha_final(cursor.getString(count++));
					dto.setFavorite(cursor.getString(count++));
					instList.add(dto);

					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("ProductDAO  -- getRecords", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}

		return instList;
	}
	public boolean updateProductSynk(SQLiteDatabase dbObject, DTO dto) {
		try {
			ProductDTO dtoObj = (ProductDTO) dto;

			String whereCls = "barcode = '" + dtoObj.getBarcode() + "'";
			ContentValues cValues = new ContentValues();
			cValues.put("barcode", dtoObj.getBarcode());
			cValues.put("name", dtoObj.getName());
			cValues.put("quantity", dtoObj.getQuantity());
			cValues.put("UOM", dtoObj.getUom());
			cValues.put("purchase_price", dtoObj.getPurchasePrice());
			cValues.put("selling_price", dtoObj.getSellingPrice());
			cValues.put("group_id", dtoObj.getGroupId());
			cValues.put("vat", dtoObj.getVat());
			cValues.put("supplier_id", dtoObj.getSupplierId());
			cValues.put("line_id", dtoObj.getLineId());
			cValues.put("active_status", dtoObj.getActiveStatus());
			cValues.put("create_date", dtoObj.getCreateDate());
			cValues.put("modified_date", dtoObj.getModifiedDate());
			cValues.put("productFlag", dtoObj.getProductFlag());
			cValues.put("sync_status", dtoObj.getSyncStatus());
			cValues.put("sub_group", dtoObj.getSubgroup());
			cValues.put("min_count_inventory", dtoObj.getMin_count_inventory());
			cValues.put("expiry_date", dtoObj.getExpiry_date());
			cValues.put("discount", dtoObj.getDiscount());
			cValues.put("fecha_inicial", dtoObj.getFecha_inicial());
			cValues.put("fecha_final", dtoObj.getFecha_final());
			cValues.put("favorite", dtoObj.getFavorite());
			dbObject.update("tblm_product", cValues, whereCls, null);
			return true;
		} catch (SQLException e) {
			Log.e("clientDAO  -- update", e.getMessage());
		} finally {
			dbObject.close();
		}
		return false;
	}

	public boolean updateProducts(SQLiteDatabase dbObject, DTO dto) {
		try {
			SelectedProddutsDTO dtoObj = (SelectedProddutsDTO) dto;

			String whereCls = "barcode = '" + dtoObj.getBarcode() + "'";
			ContentValues cValues = new ContentValues();
			cValues.put("selling_price", dtoObj.getSellPrice());
			cValues.put("purchase_price", dtoObj.getPrice());
			cValues.put("active_status", "1");

			String[] aaa = dtoObj.getQuantity().split("\\.");
			String qty = aaa[0];
			cValues.put("quantity", qty);
			cValues.put("expiry_date", dtoObj.getExpiry_date());
			cValues.put("modified_date", dtoObj.getModifiedDate());
			dbObject.update("tblm_product ", cValues, whereCls, null);

			return true;
		} catch (SQLException e) {
			Log.e("ProductDAO  -- update", e.getMessage());
		} finally {
			dbObject.close();
		}
		return false;
	}
	public boolean updateProducts2(SQLiteDatabase dbObject, DTO dto,Double act_qty,Double qty) {
		try {
			SelectedProddutsDTO dtoObj = (SelectedProddutsDTO) dto;

			String whereCls = "barcode = '" + dtoObj.getBarcode() + "'";
			ContentValues cValues = new ContentValues();
			cValues.put("selling_price", dtoObj.getSellPrice());
			cValues.put("purchase_price", dtoObj.getPrice());
			cValues.put("active_status", "1");

			cValues.put("quantity", String.valueOf(qty+act_qty));
			cValues.put("expiry_date", dtoObj.getExpiry_date());
			cValues.put("modified_date", dtoObj.getModifiedDate());
			dbObject.update("tblm_product ", cValues, whereCls, null);

			return true;
		} catch (SQLException e) {
			Log.e("ProductDAO  -- update", e.getMessage());
		} finally {
			dbObject.close();
		}
		return false;
	}



	public ProductDTO getPriceUOMByBarcode(SQLiteDatabase dbObj, String barcode) {

		ProductDTO dto = new ProductDTO();
		Cursor cursor = null;

		int count = 0;

		try {
			cursor = dbObj.rawQuery(
					"SELECT selling_price,purchase_price,uom FROM tblm_product WHERE  barcode = '" + barcode + "'",
					null);
			if (cursor.moveToFirst()) {
				do {
					dto.setSellingPrice(cursor.getString(count++));
					dto.setPurchasePrice(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("ProductDAO  -- getPriceUOMByBarcode", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}
		return dto;
	}

	public boolean update_orderproduct(SQLiteDatabase dbObject, ProductDTO dtoObj) {
		try {
			String whereCls = "product_id = '" + dtoObj.getProductId() + "'";
			ContentValues cValues = new ContentValues();
			cValues.put("barcode", dtoObj.getBarcode());
			cValues.put("name", dtoObj.getName());
			cValues.put("quantity", dtoObj.getQuantity());
			cValues.put("UOM", dtoObj.getUom());
			cValues.put("purchase_price", dtoObj.getPurchasePrice());
			cValues.put("selling_price", dtoObj.getSellingPrice());
			cValues.put("group_id", dtoObj.getGroupId());
			cValues.put("vat", dtoObj.getVat());
			cValues.put("supplier_id", dtoObj.getSupplierId());
			cValues.put("line_id", dtoObj.getLineId());
			cValues.put("active_status", dtoObj.getActiveStatus());
			cValues.put("create_date", dtoObj.getCreateDate());
			cValues.put("modified_date", dtoObj.getModifiedDate());
			cValues.put("productFlag", dtoObj.getProductFlag());
			cValues.put("sync_status", dtoObj.getSyncStatus());
			cValues.put("sub_group", dtoObj.getSubgroup());
			cValues.put("min_count_inventory", dtoObj.getMin_count_inventory());
			cValues.put("expiry_date", dtoObj.getExpiry_date());
			cValues.put("discount", dtoObj.getDiscount());
			cValues.put("fecha_inicial", dtoObj.getFecha_inicial());
			cValues.put("fecha_final", dtoObj.getFecha_final());
			dbObject.update("tblm_product", cValues, whereCls, null);
			return true;
		} catch (SQLException e) {
			Log.e("ProductDAO  -- update", e.getMessage());
		} finally {
			dbObject.close();
		}
		return false;
	}


	public Boolean Set1ActiveStatus(SQLiteDatabase dbObj) {
		try {


			String whereCls = "active_status = 0";
			ContentValues cValues = new ContentValues();
			cValues.put("active_status", "1");
			cValues.put("sync_status", "0");
			dbObj.update("tblm_product ", cValues, whereCls, null);

			return true;
		} catch (SQLException e) {
			Log.e("ProductDAO  -- update", e.getMessage());
		} finally {
			dbObj.close();
		}
		return false;
	}



}

/*******************************************************************************
 *  @author
 *  Ybrant Digital
 *  Copyright (C) Ybrant Digital
 *  http://www.ybrantdigital.com
 *******************************************************************************/
package com.servipunto.micajaplus.database.dao;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.InventoryDTO;
import com.servipunto.micajaplus.database.dto.ProductDetailsDTO;
import com.servipunto.micajaplus.database.dto.SelectedProddutsDTO;
import com.servipunto.micajaplus.utils.CommonMethods;
import com.servipunto.micajaplus.utils.Dates;
import com.servipunto.micajaplus.utils.Utils;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("LongLogTag")
public class InventoryDAO implements DAO {
	private static InventoryDAO inventoryDAO;

	private InventoryDAO() {

	}
	public boolean deleteAllRecords(SQLiteDatabase dbObj) {
		try {
			dbObj.compileStatement("DELETE  FROM tbl_inventory").execute();
			return true;
		} catch (Exception e) {
			Log.e("clientPayments DAO  -- delete", e.getMessage());
		}

		finally {
			dbObj.close();
		}
		return false;
	}

	public static InventoryDAO getInstance() {
		if (inventoryDAO == null)
			inventoryDAO = new InventoryDAO();

		return inventoryDAO;
	}
	public int Count(SQLiteDatabase dbObj) {
		Cursor cursor = null;
		int count = 0, num = 0;
		try {
			String countQuery = "SELECT COUNT(*) FROM tbl_inventory ";
			cursor = dbObj.rawQuery(countQuery, null);
			if (cursor.moveToFirst()) {
				num = cursor.getInt(count++);
			}
			return num;
		} catch (Exception e) {
			Log.e("ProductDAO  -- Count", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}
		return num;
	}
	@Override
	public boolean insert(SQLiteDatabase dbObj, List<DTO> list) {
		try {
			dbObj.beginTransaction();
			SQLiteStatement stmt = dbObj
					.compileStatement("INSERT INTO tbl_inventory(inventory_id,product_id,quantity,uom,sync_status,quantity_balance)VALUES (?,?,?,?,?,?)");

			int count = 1;

			for (DTO items : list) {
				InventoryDTO dto = (InventoryDTO) items;

				if (dto.getInventoryId() == null
						|| dto.getInventoryId().equalsIgnoreCase("")) {
					stmt.bindString(count++,
							String.valueOf(Dates.getSysDateinMilliSeconds()));
				} else {
					stmt.bindString(count++, dto.getInventoryId());
				}

				// stmt.bindString(count++,
				// String.valueOf(Dates.getSysDateinMilliSeconds()));
				stmt.bindString(count++,
						dto.getProductId() == null ? "" : dto.getProductId());
				stmt.bindString(count++,
						dto.getQuantity() == null ? "" : dto.getQuantity());
				stmt.bindString(count++,
						dto.getUom() == null ? "" : dto.getUom());
				stmt.bindLong(count++, dto.getSyncStatus());
				stmt.bindDouble(count++, dto.getQuantityBalance() == null ? 0.0:dto.getQuantityBalance());

				count = 1;

				stmt.executeInsert();
			}

			dbObj.setTransactionSuccessful();
			return true;
		} catch (Exception e) {
			Log.e("InventoryDAO  -- insert", e.getMessage());
		} finally {
			dbObj.endTransaction();
			dbObj.close();
		}
		return false;
	}

	@Override
	public boolean update(SQLiteDatabase dbObject, DTO dto) {
		try {
			InventoryDTO dtoObj = (InventoryDTO) dto;
			// product_id,quantity,uom,sync_status)VALUES (?,?,?,?)");

			String whereCls = "product_id = '" + dtoObj.getProductId() + "'";
			ContentValues cValues = new ContentValues();
			cValues.put("quantity", dtoObj.getQuantity());
			cValues.put("quantity_balance", dtoObj.getQuantityBalance());
			// cValues.put("uom", dtoObj.getUom());
			cValues.put("sync_status", dtoObj.getSyncStatus());
			dbObject.update("tbl_inventory", cValues, whereCls, null);

			return true;
		} catch (SQLException e) {
			Log.e("InventoryDAO  -- update", e.getMessage());
		} finally {
			dbObject.close();
		}
		return false;
	}
	public boolean updatelist(SQLiteDatabase dbObject,  List<DTO> list) {
		try {
			dbObject.beginTransaction();
			for (DTO dto:list) {
				InventoryDTO dtoObj = (InventoryDTO) dto;
				// product_id,quantity,uom,sync_status)VALUES (?,?,?,?)");

				String whereCls = "product_id = '" + dtoObj.getProductId() + "'";
				ContentValues cValues = new ContentValues();
				cValues.put("quantity", dtoObj.getQuantity());
				cValues.put("quantity_balance", dtoObj.getQuantityBalance());
				// cValues.put("uom", dtoObj.getUom());
				cValues.put("sync_status", dtoObj.getSyncStatus());
				dbObject.update("tbl_inventory", cValues, whereCls, null);
			}
			dbObject.setTransactionSuccessful();
			return true;
		} catch (SQLException e) {
			Log.e("InventoryDAO  -- update", e.getMessage());
		} finally {
			dbObject.endTransaction();
			dbObject.close();
		}
		return false;
	}
	public boolean update3(SQLiteDatabase dbObject, DTO dto) {
		try {
			InventoryDTO dtoObj = (InventoryDTO) dto;
			// product_id,quantity,uom,sync_status)VALUES (?,?,?,?)");

			String whereCls = "product_id = '" + dtoObj.getProductId() + "'";
			ContentValues cValues = new ContentValues();
			cValues.put("quantity", dtoObj.getQuantity());
			cValues.put("quantity_balance", dtoObj.getQuantityBalance());
//			cValues.put("quantity_balance", dtoObj.getQuantityBalance());
			cValues.put("uom", dtoObj.getUom());
			cValues.put("sync_status", dtoObj.getSyncStatus());
			dbObject.update("tbl_inventory", cValues, whereCls, null);

			return true;
		} catch (SQLException e) {
			Log.e("InventoryDAO  -- update", e.getMessage());
		} finally {
			dbObject.close();
		}
		return false;
	}

/*	public boolean getProductQuantity(SQLiteDatabase dbObject, String barcode) {
		Cursor cursor = null;

		int count = 0;

		try {
			cursor = dbObject.rawQuery("SELECT quantity FROM tbl_inventory where product_id = " + barcode, null);
			if (cursor.moveToFirst()) {
				do {


					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("InventoryDAO  -- getRecords", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObject.close();
		}

		return instList;
	}*/


	@Override
	public boolean delete(SQLiteDatabase dbObj, DTO dto) {
		InventoryDTO invenDTO = (InventoryDTO) dto;
		try {
			dbObj.compileStatement(
					"DELETE FROM tbl_inventory WHERE idProduct = '"
							+ invenDTO.getProductId() + "'").execute();

			return true;
		} catch (Exception e) {
			Log.e("InventoryDAO  -- delete", e.getMessage());
		}

		finally {
			dbObj.close();
		}
		return false;
	}

	@Override
	public List<DTO> getRecords(SQLiteDatabase dbObject) {
		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;

		int count = 0;

		try {
			cursor = dbObject.rawQuery("SELECT * FROM tbl_inventory", null);
			if (cursor.moveToFirst()) {
				do {
					InventoryDTO dto = new InventoryDTO();
					dto.setInventoryId(cursor.getString(count++));
					dto.setProductId(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));

					instList.add(dto);

					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("InventoryDAO  -- getRecords", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObject.close();
		}

		return instList;
	}
	public InventoryDTO getInventoryValue(SQLiteDatabase dbObject,String product_id){
		InventoryDTO dto = new InventoryDTO();
		Cursor cursor = null;
		int count = 0;
		try {
			cursor = dbObject.rawQuery("SELECT * FROM tbl_inventory WHERE product_id = '" + product_id + "'", null);
			if (cursor.moveToFirst()) {
				do {
					dto.setInventoryId(cursor.getString(count++));
					dto.setProductId(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));
					dto.setQuantityBalance(cursor.getDouble(count++));
					count = 0;
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("InventoryDAO  -- getRecordsWithValues", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();
			dbObject.close();
		}
		return dto;

	}

	@Override
	public List<DTO> getRecordsWithValues(SQLiteDatabase dbObject,
										  String columnName, String columnValue) {
		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;
		int count = 0;
		try {
			cursor = dbObject.rawQuery("SELECT * FROM tbl_inventory WHERE "
					+ columnName + " = '" + columnValue + "'", null);
			if (cursor.moveToFirst()) {
				do {
					InventoryDTO dto = new InventoryDTO();
					dto.setInventoryId(cursor.getString(count++));
					dto.setProductId(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));
					dto.setQuantityBalance(cursor.getDouble(count++));

					instList.add(dto);
					count = 0;
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("InventoryDAO  -- getRecordsWithValues", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();
			dbObject.close();
		}

		return instList;
	}


	public boolean IgualarInventarioProduct(SQLiteDatabase dbObj, String where_query) {
		try {
//			where_query="Where barcode != '02' and barcode != '123456'";
			dbObj.beginTransaction();
			SQLiteStatement stmt = dbObj
					.compileStatement("INSERT INTO tbl_inventory ( inventory_id, product_id, quantity, uom, sync_status, quantity_balance)\n" +
							"SELECT barcode, barcode ,quantity, uom, 0,0.0 FROM tblm_product\n" +
							where_query);

				stmt.executeInsert();

			dbObj.setTransactionSuccessful();
			return true;
		} catch (Exception e) {
			Log.e("InventoryDAO  -- insert", e.getMessage());
		} finally {
			dbObj.endTransaction();
			dbObj.close();
		}
		return false;
	}
	public List<DTO> getProductDetailsWithBarcodeWithLimit(SQLiteDatabase dbObj,int limit) {

		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;

		int count = 0;

		try {
			String query = "SELECT p.barcode,p.name,p.supplier_id,i.quantity,i.uom,p.selling_price, p.purchase_price,p.vat  FROM tbl_inventory i,tblm_product p WHERE  i.product_id = p.barcode and p.active_status=1 order by UPPER(p.name) ASC limit "+limit;

			cursor = dbObj.rawQuery(query, null);
			if (cursor.moveToFirst()) {
				do {
					ProductDetailsDTO dto = new ProductDetailsDTO();
					dto.setProductCode(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setSupplierName(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setSellingPrice(cursor.getString(count++));
					dto.setPurchasePrice(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setUtilityValue(String.valueOf(CommonMethods
							.getRoundedVal(CommonMethods.getDoubleFormate(dto
									.getSellingPrice())
									- (CommonMethods.getDoubleFormate(dto
									.getPurchasePrice())))));
					instList.add(dto);
					count = 0;
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("InventoryDAO  -- getProductDetailsWithValues",
					e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();
			dbObj.close();
		}
		return instList;

	}
public List<DTO> getProductDetailsWithBarcodeWithLimit2(SQLiteDatabase dbObj,int limit) {

		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;

		int count = 0;

		try {
			String query = "SELECT i.product_id,p.name,i.quantity,p.selling_price,p.uom, p.purchase_price,p.vat , '"
					+ "product non dish"
					+ "' as product  FROM tbl_inventory i,tblm_product p WHERE  i.product_id = p.barcode and p.active_status=1 order by UPPER(p.name) ASC limit "+limit;

			cursor = dbObj.rawQuery(query, null);
			if (cursor.moveToFirst()) {
				do {
					SelectedProddutsDTO dto = new SelectedProddutsDTO();
					dto.setIdProduct(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setSellPrice(cursor.getString(count++));
					dto.setUnits(cursor.getString(count++));
					dto.setPrice(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setProductType(cursor.getString(count++));
					dto.setUtilityValue(String.valueOf(CommonMethods
							.getRoundedVal(CommonMethods.getDoubleFormate(dto
									.getPrice())
									- (CommonMethods.getDoubleFormate(dto
									.getSellPrice())))));
					dto.setActualQty("0");

					instList.add(dto);
					count = 0;
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("InventoryDAO  -- getProductDetailsWithValues",
					e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();
			dbObj.close();
		}
		return instList;

	}
	public InventoryDTO getRecordByProductID(SQLiteDatabase dbObj,
											 String productCode) {

		InventoryDTO dto = new InventoryDTO();
		Cursor cursor = null;

		int count = 0;

		try {
			cursor = dbObj.rawQuery(
					"SELECT * FROM tbl_inventory WHERE  product_id = '"
							+ productCode + "'", null);
			if (cursor.moveToFirst()) {
				do {

					dto.setInventoryId(cursor.getString(count++));
					dto.setProductId(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setUom(cursor.getString(count++));
					dto.setSyncStatus(cursor.getInt(count++));

					count = 0;

				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("InventoryDAO  -- getRecords", e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();

			dbObj.close();
		}

		return dto;
	}
	public List<DTO> getProductDetailsWithBarcode3(SQLiteDatabase dbObj,String barcode, int limit) {

		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;

		int count = 0;

		try {
			String query = "SELECT i.product_id,p.name,i.quantity,p.selling_price,p.uom, p.purchase_price,p.vat , '"
					+ "product non dish"
					+ "' as product  FROM tbl_inventory i,tblm_product p WHERE  i.product_id = p.barcode and p.active_status=1 AND p.name like '%"+barcode+"%' order by UPPER(p.name) ASC limit "+limit;

			cursor = dbObj.rawQuery(query, null);
			if (cursor.moveToFirst()) {
				do {
					SelectedProddutsDTO dto = new SelectedProddutsDTO();
					dto.setIdProduct(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setSellPrice(cursor.getString(count++));
					dto.setUnits(cursor.getString(count++));
					dto.setPrice(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setProductType(cursor.getString(count++));
					dto.setUtilityValue(String.valueOf(CommonMethods
							.getRoundedVal(CommonMethods.getDoubleFormate(dto
									.getPrice())
									- (CommonMethods.getDoubleFormate(dto
									.getSellPrice())))));
					dto.setActualQty("0");

					instList.add(dto);
					count = 0;
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("InventoryDAO  -- getProductDetailsWithValues",
					e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();
			dbObj.close();
		}
		return instList;

	}
	public List<DTO> getProductDetailsWithBarcode2(SQLiteDatabase dbObj,String barcode, int limit) {

		List<DTO> instList = new ArrayList<DTO>();
		Cursor cursor = null;

		int count = 0;

		try {
			String query = "SELECT i.product_id,p.name,i.quantity,p.selling_price,p.uom, p.purchase_price,p.vat , '"
					+ "product non dish"
					+ "' as product  FROM tbl_inventory i,tblm_product p WHERE  i.product_id = p.barcode and p.active_status=1 AND i.product_id like '"+barcode+"%' order by UPPER(p.name) ASC limit "+limit;

			cursor = dbObj.rawQuery(query, null);
			if (cursor.moveToFirst()) {
				do {
					SelectedProddutsDTO dto = new SelectedProddutsDTO();
					dto.setIdProduct(cursor.getString(count++));
					dto.setName(cursor.getString(count++));
					dto.setQuantity(cursor.getString(count++));
					dto.setSellPrice(cursor.getString(count++));
					dto.setUnits(cursor.getString(count++));
					dto.setPrice(cursor.getString(count++));
					dto.setVat(cursor.getString(count++));
					dto.setProductType(cursor.getString(count++));
					dto.setUtilityValue(String.valueOf(CommonMethods
							.getRoundedVal(CommonMethods.getDoubleFormate(dto
									.getPrice())
									- (CommonMethods.getDoubleFormate(dto
									.getSellPrice())))));
					dto.setActualQty("0");
					instList.add(dto);
					count = 0;
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			Log.e("InventoryDAO  -- getProductDetailsWithValues",
					e.getMessage());
		} finally {
			if (cursor != null && !cursor.isClosed())
				cursor.close();
			dbObj.close();
		}
		return instList;

	}

}

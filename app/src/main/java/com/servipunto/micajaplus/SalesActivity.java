package com.servipunto.micajaplus;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.servipunto.micajaplus.adapters.RecycleAdapter_AddProduct;
import com.servipunto.micajaplus.database.DBHandler;
import com.servipunto.micajaplus.database.dao.InventoryDAO;
import com.servipunto.micajaplus.database.dao.InventoryHistoryDAO;
import com.servipunto.micajaplus.database.dao.ProductDAO;
import com.servipunto.micajaplus.database.dao.SalesDAO;
import com.servipunto.micajaplus.database.dao.SalesDetailDAO;
import com.servipunto.micajaplus.database.dao.TransaccionBoxDAO;
import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.InventoryDTO;
import com.servipunto.micajaplus.database.dto.InventoryHistoryDTO;
import com.servipunto.micajaplus.database.dto.ProductDTO;
import com.servipunto.micajaplus.database.dto.ProductDetailsDTO;
import com.servipunto.micajaplus.database.dto.SalesDTO;
import com.servipunto.micajaplus.database.dto.SalesDetailDTO;
import com.servipunto.micajaplus.database.dto.SelectedProddutsDTO;
import com.servipunto.micajaplus.database.dto.TransaccionBoxDTO;
import com.servipunto.micajaplus.interfaces.CustomItemClickListener;
import com.servipunto.micajaplus.service.SQLiteSale;
import com.servipunto.micajaplus.utils.CommonMethods;
import com.servipunto.micajaplus.utils.Constants;
import com.servipunto.micajaplus.utils.ConstantsEnum;
import com.servipunto.micajaplus.utils.Dates;
import com.servipunto.micajaplus.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class SalesActivity extends Fragment {

    public static ServiApplication appContext;
    private RecyclerView recyclerView;
    private RecycleAdapter_AddProduct mAdapter;
    SharedPreferences sharedpreferences;
    Animation startAnimation;
    private TextView total_txt;
    String total;
    private View view;
    Button btn ;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_list_products, container, false);

        appContext = (ServiApplication) getActivity().getApplicationContext();

        sharedpreferences = getActivity().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        Utils.context = getActivity().getApplicationContext();
        startAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.bounce);

/*        appContext.setSelectedproductdtolist(InventoryDAO.getInstance().getProductDetailsWithBarcodeWithLimit2(
                DBHandler.getDBObj(Constants.READABLE), 10));*/

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        total_txt = (TextView) view.findViewById(R.id.total);

        appContext.setmAdapter(new RecycleAdapter_AddProduct(getActivity(), appContext.getSelectedproductdtolist(), startAnimation, new CustomItemClickListener() {
            @Override
            public void onItemClick(View v, SelectedProddutsDTO selectedProddutsDTO) {

            }
        }));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(appContext.getmAdapter());

        return view;

    }
/*    public void vender(){
        double totals=0;
        for (int i=0;i<appContext.getSelectedproductdtolist().size();i++){
            SelectedProddutsDTO selectedproductdto = (SelectedProddutsDTO) appContext.getSelectedproductdtolist()
                    .get(i);
            totals = totals + Double.parseDouble(selectedproductdto.getActualQty());
        }
        //total:":" + debtAmount + ":" + print+ ":" + change + ":" + pay
        insertAndUpdate(totals+":0:false:0:"+totals);
    }

    private void insertAndUpdate(String payments) {
        final List<DTO> seletedProducts = appContext.getSelectedproductdtolist();
//        final ClientDTO clientDTO = appContext.getClientDTO();
        final SalesDTO salesDTO = new SalesDTO();
        final List<DTO> invoiceList = new ArrayList<>();
        final double totalAmount = Double.parseDouble(payments.split(":")[0]);
        final double discount = 0;
        final String saleID;
        final String invoice_adjnumf = "";
        final String responds_feed = "";
        final Boolean isinvoiceAdj = false;
        final double subTotalf = Double.parseDouble(payments.split(":")[0]);
        final String fecha_inicialf = Dates.getSysDate(Dates.YYYY_MM_DD_HH_MM);;
        final String fecha_finalf = "";

        saleID = String.valueOf(Dates.getSysDateinMilliSeconds());
*//*        if (tarjetachx.isChecked())
            payments=payments.concat(":"+ConstantsEnum.PAYMENT_TYPE_CARD.code());
        else*//*
            payments=payments.concat(":"+ ConstantsEnum.PAYMENT_TYPE_CASH.code());
        new SQLiteSale(sharedpreferences,payments,saleID,totalAmount,null,subTotalf,fecha_finalf,fecha_inicialf,discount,seletedProducts,isinvoiceAdj,invoiceList,invoice_adjnumf,salesDTO);
    }*/

}


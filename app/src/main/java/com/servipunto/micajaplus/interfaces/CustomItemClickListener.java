package com.servipunto.micajaplus.interfaces;

import android.view.View;

import com.servipunto.micajaplus.database.dto.SelectedProddutsDTO;

/**
 * Created by Juan on 20/05/2018.
 */

public interface CustomItemClickListener {
    public void onItemClick(View v, SelectedProddutsDTO selectedProddutsDTO);
}
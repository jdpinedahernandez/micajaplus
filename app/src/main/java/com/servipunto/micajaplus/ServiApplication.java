package com.servipunto.micajaplus;

import android.app.Application;

import com.servipunto.micajaplus.adapters.RecycleAdapter_AddProduct;
import com.servipunto.micajaplus.database.dto.DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan on 18/05/2018.
 */

public class ServiApplication extends Application {
    public static List<DTO> selectedproductdtolist = new ArrayList<DTO>();
    public static RecycleAdapter_AddProduct mAdapter;

    public static Boolean SNCRONIZAR = true;
    public static int Client_Info = 1;
    public static int CashFlowList = 2;
    public static int DishProductsList = 3;
    public static int ClientPayList = 4;
    public static int dishlist = 5;
    public static int menudishlist = 6;
    public static int inventory = 7;
    public static int inventoryAdj = 8;
    public static int inventoryHistory = 9;
    public static int lendMoney = 10;
    public static int menu = 11;
    public static int menuInventory = 12;
    public static int menuInventoryAdj = 13;
    public static int orderDetails = 14;
    public static int orders = 15;
    public static int sales = 16;
    public static int salesDetails = 17;
    public static int clientpayments = 18;
    public static int supplier = 19;
    public static int userTable = 20;
    public static int supplierpayments = 21;
    public static int updateProducts = 22;
    public static int updatepuntoredsync = 23;

    // TransaccionBox Models Names
    public static String Shop_Open_M_name = "La caja se abre";
    public static String Shop_Close_M_name = "Tienda cerrada";
    public static String Customer_Payments_M_name = "Abono del cliente a la deuda";
    public static String Customer_lend_Payments_M_name = "Préstamo de plata a cliente";
    public static String Supplier_Payments_M_name = "Pago a proveedores";
    public static String Sales_M_name = "Venta";
    public static String Cashflow_Withdraw_M_name = "Retiro de efectivo";
    public static String Cashflow_Disposit_M_name = "Abono de efectivo";
    public static String Orders_M_name = "Pedido";
    public static String Inventory_M_name = "Inventario";
    // TransaccionBox Tipoid's
    public static String Shop_Open_TipoTrans = "12";
    public static String Shop_Close_TipoTrans = "13";
    public static String Customer_PostPayments_TipoTrans = "15";
    public static String Customer_lendPayments_TipoTrans = "16";
    public static String Supplier_Payments_TipoTrans = "22";
    public static String Sales_TipoTrans = "22";
    public static String Cashflow_Withdraw_TipoTrans = "10";
    public static String Cashflow_Disposit_TipoTrans = "11";
    public static String Orders_TipoTrans = "18";
    public static String Inventory_TipoTrans = "22";
    // TransaccionBox PaymentType Names

    public static String Shop_Open_PaymentType = "La caja se abre";
    public static String Shop_Close_PaymentType = "Tienda cerrada";
    public static String Customer_Payments_PaymentType = "Pago de clientes";
    public static String Supplier_Payments_PaymentType = "Pago a proveedores";
    public static String Sales_PaymentType = "Venta";
    public static String Cashflow_Withdraw_PaymentType = "Retiro de efectivo";
    public static String Cashflow_Disposit_PaymentType = "Abono de efectivo";
    public static String Orders_PaymentType = "Pedido";
    public static String Inventory_PaymentType = "Inventario";

    public static List<DTO> getSelectedproductdtolist() {
        return selectedproductdtolist;
    }

    public static void setSelectedproductdtolist(List<DTO> selectedproductdtolist) {
        ServiApplication.selectedproductdtolist = selectedproductdtolist;
    }

    public static RecycleAdapter_AddProduct getmAdapter() {
        return mAdapter;
    }

    public static void setmAdapter(RecycleAdapter_AddProduct mAdapter) {
        ServiApplication.mAdapter = mAdapter;
    }
}

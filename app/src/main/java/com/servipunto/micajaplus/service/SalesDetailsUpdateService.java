package com.servipunto.micajaplus.service;

import android.app.IntentService;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.servipunto.micajaplus.MySingleton;
import com.servipunto.micajaplus.ServiApplication;
import com.servipunto.micajaplus.database.DBHandler;
import com.servipunto.micajaplus.database.dao.InventoryDAO;
import com.servipunto.micajaplus.database.dao.InventoryHistoryDAO;
import com.servipunto.micajaplus.database.dao.ProductDAO;
import com.servipunto.micajaplus.database.dao.SalesDAO;
import com.servipunto.micajaplus.database.dao.SalesDetailDAO;
import com.servipunto.micajaplus.database.dao.TransaccionBoxDAO;
import com.servipunto.micajaplus.database.dao.UserDetailsDAO;
import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.InventoryDTO;
import com.servipunto.micajaplus.database.dto.SalesDTO;
import com.servipunto.micajaplus.database.dto.UserDetailsDTO;
import com.servipunto.micajaplus.utils.Constants;
import com.servipunto.micajaplus.utils.RequestFiels;
import com.servipunto.micajaplus.utils.UpdateSynkStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Juan on 21/05/2018.
 */

public class SalesDetailsUpdateService extends IntentService {
    private UserDetailsDTO udto;
    SharedPreferences sharedpreferences;

    public SalesDetailsUpdateService() {
        super("SalesDetailsUpdateService");
         }
    @Override
    protected void onHandleIntent(Intent intent) {
/*        if (NetworkConnectivity
                .netWorkAvailability(SalesDetailsUpdateService.this)) {*/
        sharedpreferences = getApplicationContext().getSharedPreferences(Constants.MyPREFERENCES, Context.MODE_PRIVATE);
        udto = UserDetailsDAO.getInstance().getRecordsuser_name(DBHandler.getDBObj(Constants.READABLE),sharedpreferences.getString("username",""));
        new CickServer().execute();
/*        } else {
            ServiApplication.SNCRONIZAR = true;
        }*/


    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    class CickServer extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            // inventory
            // inventory_history
            // MenuInventoryDTO
            // customer
            //				Prueba para apartar servicios de sqlite
            //TransaccionBox
            List<DTO> transaccionBox = TransaccionBoxDAO.getInstance()
                    .getRecordsWithValues(
                            DBHandler.getDBObj(Constants.READABLE),
                            "sync_status", "0");
            //				Prueba para apartar servicios de sqlite
            List<DTO> inventorylist = InventoryDAO.getInstance()
                    .getRecordsWithValues(
                            DBHandler.getDBObj(Constants.READABLE),
                            "sync_status", "0");
            List<DTO> inventoryHistorylist = InventoryHistoryDAO.getInstance()
                    .getRecordsWithValues(
                            DBHandler.getDBObj(Constants.READABLE),
                            "sync_status", "0");

            List<DTO> sales = SalesDAO.getInstance()
                    .getRecordsWithValues(DBHandler.getDBObj(Constants.READABLE),
                            "sync_status", "0");


            List<DTO> sales_details = SalesDetailDAO.getInstance()
                    .getRecordsWithValues(DBHandler.getDBObj(Constants.READABLE),
                            "sync_status", "0");

            List<DTO> productList = ProductDAO.getInstance().getRecordsWithValues(DBHandler.getDBObj(Constants.READABLE),
                    "sync_status", "0");

            //				Prueba para apartar servicios de sqlite
            if (transaccionBox.size() > 0) {
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                        (Request.Method.POST, Constants.URL+"/sync", new RequestFiels(sharedpreferences).TransaccionBox(transaccionBox), new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt("status")==0){
                                        Log.v("Varahalababu", "Hello BABU=========>Req        "+"TransaccionBox");
                                        Log.v("Varahalababu", "Hello BABU=========>Res        "+response.toString());
                                        TransaccionBoxDAO.getInstance().deleteAllRecords(
                                                DBHandler.getDBObj(Constants.READABLE));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.i("DeleteCatalogDataSync","fail-ok");
                                }

                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO: Handle error
                                Log.i("DeleteCatalogDataSync","fail");

                            }
                        });
// Access the RequestQueue through your singleton class.
                MySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
            }
            //				Prueba para apartar servicios de sqlite
            if (sales.size() > 0) {

//				String val_sales = servicehandler.makeServiceCall(ServiApplication.URL + "/sales/create",
//				servicehandler.POST, makeJsonObject(salesdto));
                try {
                    HashMap<Integer, List<DTO>> objects = new HashMap<Integer, List<DTO>>();
                    int index = 1;
                    int step = 1;
                    for(int i=0;i<sales.size();i++){
                        if (index == 1){
                            objects.put(step, new ArrayList<DTO>() {
                            });
                        }
                        objects.get(step).add(sales.get(i));
                        index++;
                        if (index == Constants.MAX_OBJECTS){
                            index = 1;
                            step++;
                        }
                    }
                    for (Integer key : objects.keySet()) {
                        SendInfoService(new RequestFiels(sharedpreferences).getSalesData(objects.get(key)),objects,key,ServiApplication.sales,null);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            if (sales_details.size() > 0) {

                try {
                    HashMap<Integer, List<DTO>> objects = new HashMap<Integer, List<DTO>>();
                    int index = 1;
                    int step = 1;
                    for(int i=0;i<sales_details.size();i++){
                        if (index == 1){
                            objects.put(step, new ArrayList<DTO>() {
                            });
                        }
                        objects.get(step).add(sales_details.get(i));
                        index++;
                        if (index == Constants.MAX_OBJECTS){
                            index = 1;
                            step++;
                        }
                    }
                    for (Integer key : objects.keySet()) {
                        SendInfoService(new RequestFiels(sharedpreferences).getSalesDetailsData(objects.get(key)),objects,key,ServiApplication.salesDetails,null);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }


            }


            if (inventoryHistorylist.size() > 0) {

                try {
                    HashMap<Integer, List<DTO>> objects = new HashMap<Integer, List<DTO>>();
                    int index = 1;
                    int step = 1;
                    for(int i=0;i<inventoryHistorylist.size();i++){
                        if (index == 1){
                            objects.put(step, new ArrayList<DTO>() {
                            });
                        }
                        objects.get(step).add(inventoryHistorylist.get(i));
                        index++;
                        if (index == Constants.MAX_OBJECTS){
                            index = 1;
                            step++;
                        }
                    }
                    for (Integer key : objects.keySet()) {
                        SendInfoService(new RequestFiels(sharedpreferences).getInventoryHistoryTableData(objects.get(key)),objects,key,ServiApplication.inventoryHistory,null);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }


            }

            if (inventorylist.size() > 0) {

                try {
                    HashMap<Integer, List<DTO>> objects = new HashMap<Integer, List<DTO>>();
                    int index = 1;
                    int step = 1;
                    for(int i=0;i<inventorylist.size();i++){
                        if (index == 1){
                            objects.put(step, new ArrayList<DTO>() {
                            });
                        }
                        objects.get(step).add(inventorylist.get(i));
                        index++;
                        if (index == Constants.MAX_OBJECTS){
                            index = 1;
                            step++;
                        }
                    }
                    for (Integer key : objects.keySet()) {
                        SendInfoService(new RequestFiels(sharedpreferences).getInventoryTableData(objects.get(key)),objects,key,ServiApplication.inventory,inventorylist);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            if (productList.size() > 0) {
                try {
                    HashMap<Integer, List<DTO>> objects = new HashMap<Integer, List<DTO>>();
                    int index = 1;
                    int step = 1;
                    for(int i=0;i<productList.size();i++){
                        if (index == 1){
                            objects.put(step, new ArrayList<DTO>() {
                            });
                        }
                        objects.get(step).add(productList.get(i));
                        index++;
                        if (index == Constants.MAX_OBJECTS){
                            index = 1;
                            step++;
                        }
                    }
                    for (Integer key : objects.keySet()) {
                        SendInfoService(new RequestFiels(sharedpreferences).getProductTableData(objects.get(key)),objects,key,ServiApplication.updateProducts,null);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }


            }

            return null;
        }



        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            ServiApplication.SNCRONIZAR = true;
            stopSelf();
        }

    }



    private void SendInfoService(JSONObject field,HashMap<Integer, List<DTO>> objects,int key,int type,List<DTO> inventorylist){
        final List<DTO> inventorylistf = inventorylist;
        final int typef = type;
        final int keyf = key;
        final HashMap<Integer, List<DTO>> objectsf = objects;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, Constants.URL+"/sync", field, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getInt("status")==0){
                                if (typef != ServiApplication.inventory)
                                    new UpdateSynkStatus(objectsf.get(keyf),typef);
                                else{
                                    HashMap<String,Double> inventoryServer = new RequestFiels(sharedpreferences).data(response.toString());
                                    for(DTO dto: inventorylistf){
                                        InventoryDTO inventoryDTO = (InventoryDTO) dto;
                                        inventoryDTO.setQuantityBalance(0.0);
                                        if (inventoryServer.containsKey(inventoryDTO.getProductId()))
                                            inventoryDTO.setQuantity(inventoryServer.get(inventoryDTO.getProductId()).toString());
                                        InventoryDAO.getInstance().update(DBHandler.getDBObj(Constants.WRITABLE), inventoryDTO);
                                    }
                                    new UpdateSynkStatus(inventorylistf,typef);
                                }
                                System.out.println("Log: /sync: tipo trnsaccion: "+ String.valueOf(typef)+ " respose:"+ response.toString());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("Log: /sync falló: tipo trnsaccion"+ String.valueOf(typef));
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.i("DeleteCatalogDataSync","fail");

                    }
                });
// Access the RequestQueue through your singleton class.
        MySingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest);
    }


}

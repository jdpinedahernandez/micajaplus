package com.servipunto.micajaplus.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.servipunto.micajaplus.ServiApplication;
import com.servipunto.micajaplus.database.DBHandler;
import com.servipunto.micajaplus.database.dao.InventoryDAO;
import com.servipunto.micajaplus.database.dao.InventoryHistoryDAO;
import com.servipunto.micajaplus.database.dao.ProductDAO;
import com.servipunto.micajaplus.database.dao.SalesDAO;
import com.servipunto.micajaplus.database.dao.SalesDetailDAO;
import com.servipunto.micajaplus.database.dao.TransaccionBoxDAO;
import com.servipunto.micajaplus.database.dto.DTO;
import com.servipunto.micajaplus.database.dto.InventoryDTO;
import com.servipunto.micajaplus.database.dto.InventoryHistoryDTO;
import com.servipunto.micajaplus.database.dto.ProductDTO;
import com.servipunto.micajaplus.database.dto.SalesDTO;
import com.servipunto.micajaplus.database.dto.SalesDetailDTO;
import com.servipunto.micajaplus.database.dto.SelectedProddutsDTO;
import com.servipunto.micajaplus.database.dto.TransaccionBoxDTO;
import com.servipunto.micajaplus.utils.CommonMethods;
import com.servipunto.micajaplus.utils.Constants;
import com.servipunto.micajaplus.utils.Dates;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Juan on 21/05/2018.
 */

public class SQLiteSale {
    String payments;
    List<DTO> seletedProducts;
    SalesDTO salesDTO;
    double totalAmount ;
    double discount;
    String saleID;
    String responds_feed;
    Boolean isinvoiceAdj;
    double subTotalf;
    String fecha_inicialf;
    String fecha_finalf;
    List<DTO> invoiceList;
    String invoice_adjnumf;
    SharedPreferences sharedpreferences;
    Context context;
    public SQLiteSale(Context context, SharedPreferences sharedpreferences, String payments, List<DTO> seletedProducts) {
        this.sharedpreferences = sharedpreferences;
        this.payments = payments;
        this.saleID = String.valueOf(Dates.getSysDateinMilliSeconds());
        this.totalAmount = Double.parseDouble(payments.split(":")[0]);
        this.subTotalf = Double.parseDouble(payments.split(":")[0]);
        this.fecha_finalf = "";
        this.fecha_inicialf =  Dates.getSysDate(Dates.YYYY_MM_DD_HH_MM);
        this.discount = 0;
        this.seletedProducts = seletedProducts;
        this.isinvoiceAdj = false;
        this.invoiceList = new ArrayList<>();
        this.invoice_adjnumf = "";
        this.salesDTO = new SalesDTO();
        this.context = context;
        new SQLiteAsync().execute();
    }

    private class SQLiteAsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            updateMenuAndDishInventory(seletedProducts);
            validateAndInsert(payments,saleID,totalAmount,subTotalf,fecha_finalf,fecha_inicialf,discount,seletedProducts,isinvoiceAdj);
//            InvoiceDetailsDAO.getInstance().deleteAllRecords(DBHandler.getDBObj(Constants.WRITABLE));
            return "";
        }

        @Override
        protected void onPostExecute(String estado_final) {
            Intent intent = new Intent(context, SalesDetailsUpdateService.class);
            context.startService(intent);
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
    private void updateMenuAndDishInventory(List<DTO> seletedProducts) {
/*Juan Agilizar SQLite*/
        List<DTO> listInventoryDTO = new ArrayList<DTO>();
        List<DTO> list = new ArrayList<DTO>();
/*Juan Agilizar SQLite*/
        // MemoryProducts - Harold
        //List <DTO> tempList = appContext.getProductDetails();
        for (int i = 0; i < seletedProducts.size(); i++) {
            SelectedProddutsDTO selectedDTO = (SelectedProddutsDTO) seletedProducts.get(i);

            InventoryDTO inDTO = InventoryDAO.getInstance()
                    .getRecordByProductID(DBHandler.getDBObj(Constants.READABLE), selectedDTO.getIdProduct());
            String qty = "";
            if (inDTO.getQuantity() != null && selectedDTO.getQuantity() != null)
                qty = CommonMethods.UOMConversions(inDTO.getQuantity(), inDTO.getUom(), selectedDTO.getQuantity(),
                        selectedDTO.getUnits(), "1", "");

            inDTO.setProductId(selectedDTO.getIdProduct());

				/*
				Harold: Determina si un producto proviene del Precargue para que sea tenido en cuenta
				en la Próxima Sincronización
				 */
            if (inDTO.getQuantity().equals("0"))
                updateNewProduct(selectedDTO);
            inDTO.setQuantity(qty);
            if (selectedDTO.getBarcode()!=null)
                inDTO.setQuantityBalance(CommonMethods.getBalanceQuantity(selectedDTO.getBarcode(),"-"+selectedDTO.getQuantity()));
            else
                inDTO.setQuantityBalance(CommonMethods.getBalanceQuantity(selectedDTO.getIdProduct(),"-"+selectedDTO.getQuantity()));
            inDTO.setUom(selectedDTO.getUnits());
            inDTO.setSyncStatus(0);
				/*Juan Agilizar SQLite*/
            listInventoryDTO.add(inDTO);
//				InventoryDAO.getInstance().update(DBHandler.getDBObj(Constants.WRITABLE), inDTO);
				/*Juan Agilizar SQLite*/

				/*Juan Agilizar SQLite*/
//				List<DTO> list = new ArrayList<DTO>();
				/*Juan Agilizar SQLite*/
            InventoryHistoryDTO hDTO = new InventoryHistoryDTO();
            ProductDTO productDto = ProductDAO.getInstance()
                    .getRecordsByProductID(DBHandler.getDBObj(Constants.READABLE), selectedDTO.getIdProduct());

            hDTO.setDateTime(Dates.getSysDate(Dates.YYYY_MM_DD_HH_MM));
            hDTO.setProductId(selectedDTO.getIdProduct());
            hDTO.setQuantity(qty);
            hDTO.setPrice(CommonMethods.getDoubleFormate(productDto.getSellingPrice()));
            hDTO.setUom(selectedDTO.getUnits());
            hDTO.setSyncStatus(Constants.FALSE);
            hDTO.setInvoiceNum(inDTO.getInventoryId());
            list.add(hDTO);

        }
        if (listInventoryDTO.size()>0)
            InventoryDAO.getInstance().updatelist(DBHandler.getDBObj(Constants.WRITABLE), listInventoryDTO);
        if (list.size()>0)
            InventoryHistoryDAO.getInstance().insert(DBHandler.getDBObj(Constants.WRITABLE), list);

    }
    private void updateNewProduct(SelectedProddutsDTO selectedDTO) {
        ProductDTO productDTOTemp = ProductDAO.getInstance().getRecordsByBarcode(DBHandler.getDBObj(Constants.READABLE),
                selectedDTO.getIdProduct());
        productDTOTemp.setSyncStatus(0);
        productDTOTemp.setModifiedDate(Dates.getSysDate(Dates.YYYY_MM_DD_HH_MM));
        ProductDAO.getInstance().update(DBHandler.getDBObj(Constants.WRITABLE), productDTOTemp);
    }
    private void validateAndInsert(String Payments,String saleID,double totalAmount,
                                   double subTotalf,String fecha_finalf,String fecha_inicialf,double discount,
                                   List<DTO> seletedProducts,Boolean isInvoiceAdj) {
        if (SalesDAO.getInstance().insert(DBHandler.getDBObj(Constants.WRITABLE), getSalesList(Payments,saleID,totalAmount,subTotalf,fecha_finalf,fecha_inicialf,discount))) {
            getCallTransaccionBoxService(ServiApplication.Sales_M_name, ServiApplication.Sales_TipoTrans,
                    ServiApplication.Sales_PaymentType);
            if (SalesDetailDAO.getInstance().insert(DBHandler.getDBObj(Constants.WRITABLE),
                    getSalesDetailsList(Payments,seletedProducts,saleID,isInvoiceAdj))) {

            }
        }
    }
    private List<DTO> getSalesList(String payments,String saleID,double totalAmount,
                                   double subTotal,String fecha_final,String fecha_inicial,double discount) {
        fecha_final = Dates.getSysDate(Dates.YYYY_MM_DD_HH_MM);
        List<DTO> list = new ArrayList<DTO>();

        SalesDTO saleDTO = new SalesDTO();

        saleDTO.setSaleID(saleID);
        saleDTO.setDateTime(Dates.getSysDate(Dates.YYYY_MM_DD_HH_MM));
        saleDTO.setInvoiceNumber(saleID);

        System.out.println("Log: Making a sale: Gross: " + subTotal + " Net: " + totalAmount);

        saleDTO.setGrossAmount(String.valueOf(subTotal));
        saleDTO.setNetAmount(String.valueOf(totalAmount));
        saleDTO.setPaymentType(payments.split(":")[5]);
        saleDTO.setAmountPaid(payments.split(":")[4]);
        saleDTO.setDiscount(String.valueOf(discount));
        saleDTO.setFecha_inicial(fecha_inicial);
        saleDTO.setFecha_final(fecha_final);

/*        if (clientDTO != null)
            saleDTO.setClientId(clientDTO.getCedula());
        else*/
        saleDTO.setClientId("");

        saleDTO.setSyncStatus(0);

        list.add(saleDTO);

        return list;
    }
    private void getCallTransaccionBoxService(String module_nem, String tipo, String PaymentType) {

        try {
            Log.v("varahalababu", "varahalabau======test============= sales");
            List<DTO> list = SalesDAO.getInstance().getRecords(DBHandler.getDBObj(Constants.READABLE));
            SalesDTO saledto = (SalesDTO) list.get(list.size() - 1);
            List<DTO> dto = new ArrayList<DTO>();

            TransaccionBoxDTO tdto = new TransaccionBoxDTO();
            tdto.setAmount(saledto.getAmountPaid());
            tdto.setModule_name(module_nem);
            tdto.setSyncstatus(0);
            tdto.setTipo_transction(tipo);
            tdto.setTransaction_type(PaymentType);
            tdto.setDatetime(Dates.getSysDate(Dates.YYYY_MM_DD_HH_MM));
            tdto.setUsername(sharedpreferences.getString("user_name", ""));
            tdto.setStore_code(sharedpreferences.getString("store_code", ""));
            dto.add(tdto);

/*            if (saledto.getClientId().length() > 2) {
                TransaccionBoxDTO tdto1 = new TransaccionBoxDTO();
                tdto1.setAmount(saledto.getNetAmount());
                tdto1.setModule_name(ServiApplication.Customer_lend_Payments_M_name);
                tdto1.setSyncstatus(0);
                tdto1.setTipo_transction(ServiApplication.Customer_lendPayments_TipoTrans);
                tdto1.setTransaction_type(ServiApplication.Customer_Payments_PaymentType);
                tdto1.setDatetime(Dates.getSysDate(Dates.YYYY_MM_DD_HH_MM));
                tdto1.setUsername(sharedpreferences.getString("user_name", ""));
                tdto1.setStore_code(sharedpreferences.getString("store_code", ""));
                dto.add(tdto1);


            }*/

            //SincronizarTransaccionesDAO.getInstance().insert(DBHandler.getDBObj(Constants.WRITABLE), list);

            if (TransaccionBoxDAO.getInstance().insert(DBHandler.getDBObj(Constants.WRITABLE), dto)) {
//				Prueba para apartar servicios de sqlite
				/*if (NetworkConnectivity.netWorkAvailability(getApplicationContext())) {
					Log.d("SYNC","sync...sale...trx");
					Intent intent = new Intent(SalesActivity.this, TransaccionBoxService.class);
					startService(intent);
				} else {
					Log.v("varahalababu", "varahalabau======test============= sales else transation");
				}*/
//				Prueba para apartar servicios de sqlite

            }

        }catch(Exception ex){
            Log.d("ERROR ON SALE", ex.getMessage());
        }finally {
            Log.d("Sync Sale Trx","Sync....");
//				Prueba para apartar servicios de sqlite
/*			Intent intent = new Intent(SalesActivity.this,
					SincronizarTransacciones.class);
			startService(intent);*/
//				Prueba para apartar servicios de sqlite
        }
    }
    private List<DTO> getSalesDetailsList(String payments,List<DTO> seletedProducts,String saleID,Boolean isInvoiceAdj) {
        List<DTO> list = new ArrayList<DTO>();
        List<DTO> selectedProdList;

        selectedProdList = seletedProducts;

        for (int i = 0; i < selectedProdList.size(); i++) {
            SalesDetailDTO detailsDTO = new SalesDetailDTO();

            SelectedProddutsDTO dto = (SelectedProddutsDTO) selectedProdList.get(i);
			/*
			Harold: Valida si el precio cambio y lo cambia en la BD
			 */
            validateIfSellPriceChange(dto);
            //END

            detailsDTO.setSaleId(saleID);
            detailsDTO.setProductId(dto.getIdProduct());
            detailsDTO.setCount(dto.getQuantity());
            detailsDTO.setUom(dto.getUnits());
            detailsDTO.setPrice(dto.getSellPrice());
            detailsDTO.setDishId("");
            detailsDTO.setSyncStatus(0);

            list.add(detailsDTO);
        }
        return list;
    }
    private void validateIfSellPriceChange(SelectedProddutsDTO product) {
        double utility = 0.0, utilityTemp = 0.0;
        try {
            utilityTemp = CommonMethods.getDoubleFormate(product.getSellPrice()) - CommonMethods.getDoubleFormate(product.getPrice());
            utility = CommonMethods.getDoubleFormate(product.getUtilityValue());

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (utilityTemp != utility) {

            String productBarcode = (product.getBarcode() == null) ? product.getIdProduct() : product.getBarcode();
            ProductDTO dtoo = ProductDAO.getInstance().getRecordsByBarcode(
                    DBHandler.getDBObj(Constants.READABLE), productBarcode);



            dtoo.setSellingPrice(product.getSellPrice());
            dtoo.setSyncStatus(0);
            ProductDAO.getInstance().updateProductSynk(
                    DBHandler.getDBObj(Constants.WRITABLE), dtoo);
        }
    }

}

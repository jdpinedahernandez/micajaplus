package com.servipunto.micajaplus.service;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.servipunto.micajaplus.ContentActivity;
import com.servipunto.micajaplus.MainActivity;
import com.servipunto.micajaplus.MySingleton;
import com.servipunto.micajaplus.SalesActivity;
import com.servipunto.micajaplus.database.DBHandler;
import com.servipunto.micajaplus.database.dao.InventoryDAO;
import com.servipunto.micajaplus.database.dao.ProductDAO;
import com.servipunto.micajaplus.database.dao.UserDetailsDAO;
import com.servipunto.micajaplus.database.dao.UserModuleIDDAO;
import com.servipunto.micajaplus.json.Inventory;
import com.servipunto.micajaplus.json.Product;
import com.servipunto.micajaplus.json.User;
import com.servipunto.micajaplus.utils.Constants;
import com.servipunto.micajaplus.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Juan on 15/05/2018.
 */

public class LoginService {
    SharedPreferences sharedpreferences;
    public LoginService(Context context, String response, SharedPreferences.Editor editor,SharedPreferences sharedpreferences) {
        this.sharedpreferences=sharedpreferences;
        new SQL_User(response,context,editor).execute();
    }

    public class SQL_User extends AsyncTask<Void, Void, Void> {
        String response;
        Context context;
        SharedPreferences.Editor editor;

        public SQL_User(String response,Context context,SharedPreferences.Editor editor) {
            this.context = context;
            this.response=response;
            this.editor = editor;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            CommonMethods.showProgressDialog(getString(R.string.db_cleanup),LoginActivity.this);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Utils.context = context;
            UserModuleIDDAO.getInstance().deleteAllRecords(DBHandler.getDBObj(Constants.WRITABLE));
            UserDetailsDAO.getInstance().deleteUser(DBHandler.getDBObj(Constants.WRITABLE));
            InventoryDAO.getInstance().deleteAllRecords(DBHandler.getDBObj(Constants.WRITABLE));
            ProductDAO.getInstance().deleteAllRecords(DBHandler.getDBObj(Constants.WRITABLE));

            if (UserDetailsDAO.getInstance().insert(DBHandler.getDBObj(Constants.WRITABLE), User.setDataUserInfo(response,editor))) {
                UserModuleIDDAO.getInstance().insert(DBHandler.getDBObj(Constants.WRITABLE),User.getModulesData(response));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            Log.i("SQL_User","ok");
            DeleteCatalogDataSync(context);
//            new SyncDataFromServer(LoginActivity.this, 0);
        }
    }
    private void DeleteCatalogDataSync(final Context context){
        String store_code = sharedpreferences.getString("store_code","");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, Constants.URL+"/catalog/clear/"+store_code, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("msg").equalsIgnoreCase("Data deletion successfull!")){
                                Log.i("DeleteCatalogDataSync","ok");
                                InventoryFromCentralServer(1,context);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.i("DeleteCatalogDataSync","fail-ok");
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.i("DeleteCatalogDataSync","fail");

                    }
                });
// Access the RequestQueue through your singleton class.
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }
    public void InventoryFromCentralServer(int next_page, final Context context){
        Map<String, String> params = new HashMap();
        params.put("name", "inventory");
        params.put("username", sharedpreferences.getString("username",""));
        params.put("store_code", sharedpreferences.getString("store_code",""));

        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, Constants.URL+"/sync-client?page="+next_page, parameters, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getJSONArray("data").length()!=0){
                                Log.i("InventoryFromCentral","ok");
                                try {
                                    InventoryDAO.getInstance().insert(DBHandler.getDBObj(Constants.WRITABLE),
                                            Inventory.getinventory(response.toString()));
                                    InventoryFromCentralServer(response.getJSONObject("paginator").getInt("next_page"),context);
                                } catch (JSONException e) {
                                    ProductFromCentralServer(1,context);
                                    e.printStackTrace();
                                    Log.i("InventoryFromCentral","ok-final");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.i("InventoryFromCentral","fail");
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.i("DeleteCatalogDataSync","fail");

                    }
                });
    // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }
    public void ProductFromCentralServer(int next_page, final Context context){
        Map<String, String> params = new HashMap();
        params.put("name", "product");
        params.put("username", "307121");
        params.put("store_code", "30712");

        JSONObject parameters = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.POST, Constants.URL+"/sync-client?page="+next_page, parameters, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getJSONArray("data").length()!=0){
                                Log.i("ProductFromCentral","ok");
                                try {
                                    ProductDAO.getInstance().insert(DBHandler.getDBObj(Constants.WRITABLE),
                                            Product.getProductData(response.toString()));
                                    ProductFromCentralServer(response.getJSONObject("paginator").getInt("next_page"),context);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.i("ProductFromCentral","ok-final");
                                    Intent intent = new Intent(context, ContentActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(intent);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.i("ProductFromCentral","fail");
                        }

                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.i("DeleteCatalogDataSync","fail");

                    }
                });
        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }
}
